<?php

$data = array(
    array(
        "id" => 1,
        "col" => 1,
        "row" => 1,
        "size_x" => 1,
        "size_y" => 2,
        "title" => '',
        "text" => 'A campanha teve <strong>1.498.415</strong> de engajamento. 95% das interações vieram a partir do YouTube.',
        "image" => '',
        "icon" => 'circle__image fa fa-file-image-o fa-3x',
        "comments" => Array(

        )
    ),
    array(
        "id" => 1,
        "col" => 2,
        "row" => 1,
        "size_x" => 1,
        "size_y" => 2,
        "title" => '',
        "text" => 'A campanha gerou aproximadamente 22,32% de comentários positivos, 77,18% neutros e 0,5% negativos.',
        "image" => '',
        "icon" => 'circle__image fa fa-file-image-o fa-3x',
        "comments" => Array(

        )
    ),
    array(
        "id" => 2,
        "col" => 1,
        "row" => 3,
        "size_x" => 2,
        "size_y" => 1,
        "title" => 'avaliação',
        "text" => 'A campanha obteve um engajamento bem alto. Grande parte desse sucesso deve-se ao fato dos conteúdos estarem com a linguaguem dos infuenciadores, soando bem natural. O público percebeu isso e entrou na brincadeira, se divertindo com a marca.',
        "image" => '',
        "icon" => 'circle__image fa fa-file-image-o fa-3x',
        "comments" => Array(

        )
    ),
    array(
        "id" => 2,
        "col" => 3,
        "row" => 1,
        "size_x" => 2,
        "size_y" => 3,
        "title" => 'observação',
        "text" => 'O público se interessou bastante pelo produto, os que já consomem publicaram elogios e enalteceram o chocolate e os que não conhecem demonstraram estar esperando uma oportunidade para experimentar. <br><br>Um grupo de pessoas também comentou que, apesar de adorar o chocolate, tem dificuldades de encontra-lo, abrindo espaço para marca interagir e explicar melhor sobre a distribuição do <strong>5Star</strong>.<br><br><strong>Comentários de exemplo:</strong><center>',
        "image" => '',
        "icon" => 'circle__image fa fa-file-image-o fa-3x',
        "comments" => Array(
            Array(
                "image" => 'https://graph.facebook.com/1102470443109927/picture',
                "text" => 'texto do comentário',
                "name" => '@User'
            ),
            Array(
                "image" => 'https://graph.facebook.com/1102470443109927/picture',
                "text" => 'texto do comentário',
                "name" => '@User'
            )
        )
    )
);

echo json_encode($data);

?>
