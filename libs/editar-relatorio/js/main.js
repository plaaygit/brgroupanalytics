// VARIAVEIS GLOBAIS
var gridster , gridster_json, serializations, item;

// ON READY
// -------------------------------------------------------------------------------------------------
$(document).ready(function(){

    var gridEditor = new grid();
    gridEditor.init();
    gridEditor.get();
    gridEditor.add(".er-btn__box");
    gridEditor.save("#er-save");

    var editor = new edit();
    editor.init();
    editor.edit(".er-dragdrop__item-text");
    editor.edit(".er-dragdrop__item-title");
    editor.edit(".er-dragdrop__item-picture");

    var icon = new font();
    icon.init();
    icon.add();
    icon.edit(".er-dragdrop__item-icon");

    var comment = new comments();
    comment.init();
    comment.add();

    var menu = new menus();
    menu.init();
    menu.menu();
    menu.add('.head','<br /><h1 class="er-dragdrop__item-title">Titulo</h1>');
    menu.add('.para','<p class="er-dragdrop__item-text">Insira seu texto aqui</p>');
    menu.close();

    var modal = new modals();
    modal.close('#er-close-modal');
    modal.open('#er-add-comment','.er-comment');
    modal.close('#er-add-item-comment');
    modal.open('.er-dragdrop__item-card','.er-comment-delete');
    modal.close('#er-del-comment');
    modal.open('.icon','.er-font-icon');
    modal.close('#er-add-icon');

});
