/**
/* GRIDSTER
/* ----------------------------------------------------------------------------------------------------------------------------
/**/
var grid = function(){

    // TEMPLATE MENU BOX
    var menu_box = '<div class="er-menu__box"><i class="fa fa-plus-square-o"></i>'+
        '<ul class="er-menu__box-list">'+
        //'<li id="er-add-comment"><i class="fa fa-comments-o"></i> comentários</li>'+
        '<li id="er-box-delete"><i class="fa fa-trash"></i> excluir</li>'+
        '</ul>'+
        '</div>';

    // CALLBACK RESIZE
    var update_box = function(btn) {
        $.each(gridster.gridmap,function(index){
            var row_limit = [];
            if(typeof gridster.gridmap[index] != 'undefined'){
                $.each(gridster.gridmap[index],function(key,value){
                    if( typeof value === 'object'){
                        row_limit = key;
                    };
                });
                if(row_limit > 3){
                    $.each(gridster.$widgets,function(cont){
                        gridster.resize_widget($(gridster.$widgets[cont]),1,1);
                    });
                    $('.er-alert').text('o box ultrapassa a area limite, reorganize')
                        .fadeIn();
                    setTimeout(function(){
                        $('.er-alert').fadeOut();
                    },3000);
                }
            };
        });
    };

    // INIT GRID
    function init(){
        gridster = $("#er-area").gridster({
            widget_base_dimensions: [240, 150],
            widget_margins: [10, 10],
            serialize_params: function($w, wgd)
            {
                var comments = [] ;
                $($w).find('.er-dragdrop__item-card').each(function(){
                    comments.push({
                        image: $(this).find('img').attr('src'),
                        text: $(this).find('.card__text').text(),
                        name: $(this).find('.card__name').text(),
                    });
                });
                return {
                    id: parseFloat($($w).attr('id')),
                    col: wgd.col,
                    row: wgd.row,
                    size_x: wgd.size_x,
                    size_y: wgd.size_y,
                    title: $($w).find('.er-dragdrop__item-title').html(),
                    text: $($w).find('.er-dragdrop__item-text').html(),
                    image: $($w).find('.er-dragdrop__item-image').attr('src'),
                    icon: $($w).find('.er-dragdrop__item-icon i').attr('class'),
                    comments
                };
            },
            draggable: {
                stop: update_box
            },
            resize: {
                enabled: true,
                max_size: [4, 3],
                stop: update_box
            }
        }).data('gridster');
        getSave();
        cleanBox();
    };

    // DADOS PRESETS
    function getData(){
        gridster_json = [
            {
                "id": 4,
                "size_x": 1,
                "size_y": 2,
                "text": 'Insira seu texto aqui',
                "icon": "circle__image fa fa-file-image-o fa-3x"
            },
            {
                "id": 2,
                "size_x": 2,
                "size_y": 1,
                "title":'Titulo',
                "text": 'Insira seu texto aqui'
            },
            {
                "id": 3,
                "size_x": 2,
                "size_y": 3,
                "image": 'http://placehold.it/800?text=IMAGE'
            },
            {
                "id": 1,
                "size_x": 2,
                "size_y": 3,
                "title": 'Titulo',
                "text": 'Insira seu texto aqui',
            }
        ];
        serializations = Gridster.sort_by_row_and_col_asc( gridster_json );
    };

    // SALVO
    function getLoad(idcampanha){
        var dataLoad = {
            campanha: idcampanha
        };
        $.get('http://relatorios.plaaymedia.com/pages/ajax/editar-campanha.php',dataLoad, function(response) {

            gridster_json = response;
            var testeee = eval(gridster_json)
            console.log(testeee);
            serializations = Gridster.sort_by_row_and_col_asc( testeee );
            gridster.remove_all_widgets();
            $.each(serializations, function() {
                if ('key' in this !== 'null' && 'key' in this !== 'undefined') {
                    gridster.add_widget(gridster_tipo(this.id, this) , this.size_x, this.size_y, this.col, this.row);
                } else {
                    console.log("Erro ao carregar dados");
                }
            });
            if(gridster.$widgets.length <= 0){
                $('#er-save').prop('disabled',true);
            } else {
                $('#er-save').prop('disabled',false);
                getData();
            }
        });
    }

    function getSave(){
        $('#er-load').click(function(){
            getLoad($(this).data('idcampanha'));
        });
    }

    // POPULAR COMENTÁRIOS
    function comment(data){
        var card =[],box_card='';
        if(data.comments && data.comments.length >= 0){
            $.each(data.comments,function(key,value){
                card.push('<figure class="er-card er-dragdrop__item-card">'+
                          '<img class="er-card__image circle__image" src="'+ data.comments[key].image +'" alt="" width="58" height="58">'+
                          '<figcaption class="er-card__caption">'+
                          '<h3 class="er-card__name">'+ data.comments[key].name +'</h3>'+
                          '<p class="er-card__text">'+ data.comments[key].text +'</p>'+
                          '</figcaption>'+
                          '</figure>');
            });
            for(var i=0,l = card.length;i < l;i++){
                box_card += card[i];
            }
        };
        return box_card;
    };

    // SWITCH DOS BOX ESCOLHIDO
    function gridster_tipo(tipo,dado){
        switch(tipo) {
            case 4:
                return '<li class="er-dragdrop__item" id="'+ dado.id +'">'+
                    '<div class="er-dragdrop__box">'+
                    menu_box +
                    '<div class="er-dragdrop__item-icon"><i class="'+ dado.icon +'"></i></div>'+
                    '<p class="er-dragdrop__item-text">'+ dado.text +'</p>'+
                    comment(dado)+
                    '</div>'+
                    '</li>'
                break;
            case 2:
                return '<li class="er-dragdrop__item" id="'+ dado.id +'">'+
                    '<div class="er-dragdrop__box">'+
                    menu_box +
                    '<h3 class="er-dragdrop__item-title">'+ dado.title +'</h3>'+
                    '<p class="er-dragdrop__item-text">'+ dado.text +'</p>'+
                    comment(dado)+
                    '</div>'+
                    '</li>'
                break;
            case 3:
                return '<li class="er-dragdrop__item" id="'+ dado.id +'">'+
                    '<div class="er-dragdrop__box">'+
                    menu_box +
                    '<div class="er-dragdrop__item-picture">'+
                    '<img class="er-dragdrop__item-image" src="'+ dado.image +'" />'+
                    '</div>'+
                    comment(dado)+
                    '</div>'+
                    '</li>'
                break;
            case 1:
                return '<li class="er-dragdrop__item" id="'+ dado.id +'">'+
                    '<div class="er-dragdrop__box">'+
                    menu_box +
                    '<h3 class="er-dragdrop__item-title">'+ dado.title +'</h3>'+
                    '<p class="er-dragdrop__item-text">'+ dado.text +'</p>'+
                    comment(dado)+
                    '</div>'+
                    '</li>'
                break;
            default:
                console.log("Tipo indefinido");
        }
    };

    // INSERIR BOX ESCOLHIDO
    function gridster_add( number ){
        var item = serializations[number];
        var row_limit = [];
        if(gridster.$widgets.length < 0){
            $('#er-save').prop('disabled',true);
        } else {
            $('#er-save').prop('disabled',false);
        }
        $.each(gridster.gridmap,function(index){
            if(typeof gridster.gridmap[index] != 'undefined'){
                $.each(gridster.gridmap[index],function(key,value){
                    if( typeof value === 'object'){
                        row_limit = key;
                    };
                });
                if(row_limit > 3){
                    $.each(gridster.$widgets,function(cont){
                        if (cont === gridster.$widgets.length-1){
                            gridster.resize_widget($(gridster.$widgets[cont]),1,1);
                        }
                    });
                };
            };
        });
        var contw = gridster.gridmap.length - 1;
        var tt;
        $.each(gridster.gridmap[contw],function(key,value){
            if( typeof value === 'object'){
                tt = key;
            };
        });
        if (tt === 3 ) {
            $('.er-alert').text('limite de boxes atingido')
                .fadeIn();
            setTimeout(function(){
                $('.er-alert').fadeOut();
            },3000);
        }else{
            gridster.add_widget(gridster_tipo(item.id, item), item.size_x, item.size_y);
        };
    };

    // INSERIR BOX
    function addBox(btn){
        $(btn).click(function(){
            gridster_add($(this).data("box"));
        });
    };

    // LIMPAR
    function cleanBox(){
        $('#er-erase').click(function(){
            gridster.remove_all_widgets();
            if(gridster.$widgets.length <= 0){
                $('#er-save').prop('disabled',true);
            } else {
                $('#er-save').prop('disabled',false);
                getData();
            }
            $('.er-alert').text('Área limpa!')
                .fadeIn();
            setTimeout(function(){
                $('.er-alert').fadeOut();
            },3000);
        });
    };

    // SALVAR
    function save(btn){
        $(btn).click(function(){
            var row_limit = [];
            $.each(gridster.gridmap,function(index){
                if(typeof gridster.gridmap[index] != 'undefined'){
                    $.each(gridster.gridmap[index],function(key,value){
                        if( typeof value === 'object'){
                            row_limit = key;
                        };
                    });
                };
            });
            if(row_limit > 3){
                $('.er-alert').text('Há boxes fora do limite!')
                    .fadeIn();
                setTimeout(function(){
                    $('.er-alert').fadeOut();
                },3000);
            } else {
                var save = {
                    "campanha": $(this).data('idcampanha'),
                    "item": JSON.stringify(gridster.serialize())
                }
                $.post('http://relatorios.plaaymedia.com/pages/ajax/editar-campanha.php',save).success(function(data){
                    $('.er-alert').text('Salvo com sucesso!')
                        .fadeIn();
                    setTimeout(function(){
                        $('.er-alert').fadeOut();
                    },3000);
                });
            }
        });
    };

    // FUNÇÕES DA CLASSE
    return {
        init: init,
        get: getData,
        add: addBox,
        save: save,
        load: getLoad
    }

};
