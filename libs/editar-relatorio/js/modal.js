/**
/* MODAL
/* ----------------------------------------------------------------------------------------------------------------------------
/**/
var modals = function(){

    function open(btn,item){
        $(document).on('click',btn,function(){
            $('.er-modal').css('visibility','visible');
            $(item).show();
        });
    };

    function close(item){
        $(document).on('click',item,function(){
            $('.er-modal').css('visibility','hidden');
            $('.er-modal__item').hide();
        });
    };

    return {
        open: open,
        close: close
    }

};
