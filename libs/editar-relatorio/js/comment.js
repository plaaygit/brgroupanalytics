/**
/* COMMENTS
/* ----------------------------------------------------------------------------------------------------------------------------
/**/
var comments = function(){

    // DADOS
    function getData(){
        $.get('http://localhost/feature/data/comment.php', function(response) {
            comment = JSON.parse(response);
            $.each(comment,function(index){
                $('.er-comment__list').append('<li class="er-comment__card" data-comment="'+ (index + 1) +'">'+
                                              '<figure class="er-card er-dragdrop__item-card">'+
                                              '<img class="er-card__image circle__image" src="'+ comment[index].picture +'" alt="" width="58" height="58">'+
                                              '<figcaption class="er-card__caption">'+
                                              '<h3 class="er-card__name">'+ comment[index].name +'</h3>'+
                                              '<p class="er-card__text">'+ comment[index].text +'</p>'+
                                              '</figcaption>'+
                                              '</figure>'+
                                              '</li>');
            });
        });
    };

    // ABRIR COMENTARIOS
    function open(){
        getData();
        $(document).on("click","#er-add-comment",function(){
            item = $(this).closest('.er-dragdrop__box');
            $('.er-comment__card').hide();
            $('.er-card').removeClass('er-dragdrop__item-card');
            $('.er-comment__point').html('');
            $('.er-comment__card').each(function(i){
                $('.er-comment__point').append('<li class="er-comment__point--item" data-type="'+ (i+1) +'" />');
            });
            $('.er-comment__card[data-comment="1"]').show();
            $('.er-comment__point--item[data-type="1"]').addClass('er-comment__point--item--active');
        });
        choice();
        del();
    };

    // SELECIONAR COMENTARIOS
    function choice(){
        $(document).on("click",".er-comment__point--item",function(){
            $('.er-comment__card').hide();
            $('.er-comment__point--item').removeClass('er-comment__point--item--active');
            $('.er-comment__card[data-comment="'+ $(this).data('type') +'"]').show();
            $('.er-comment__point--item[data-type="'+ $(this).data('type') +'"]').addClass('er-comment__point--item--active');
        });
    }

    // ADICIONAR COMENTARIOS
    function add(){
        $(document).on("click","#er-add-item-comment",function(){
            $('.er-card').addClass('er-dragdrop__item-card');
            $(item).append($('.er-comment__card:visible').html());
        });
    }

    function del(){
        $(document).on("click",".er-dragdrop__item-card",function(){
            item = $(this);
        });
        $('#er-del-comment').click(function(){
            $(item).remove();
        });
        $(document).on("mouseenter",".er-dragdrop__item-card",function(){
            $(this).addClass('er-del-on');
        });
        $(document).on("mouseleave",".er-dragdrop__item-card",function(){
            $(this).removeClass('er-del-on');
        });
    };

    return {
        init: open,
        add: add
    }

};
