/**
/* MENU
/* ----------------------------------------------------------------------------------------------------------------------------
/**/
var menus = function(){

    // MENU
    function init(){
        $('.er-menu__item').mouseenter(function(){
            $(this).addClass('er-menu__item--active');
            $(".er-menu__option").addClass('er-menu__option--active');
        });
        $('.er-menu__option').mouseleave(function(){
            $('.er-menu__item').removeClass('er-menu__item--active');
            $(".er-menu__option").removeClass('er-menu__option--active');
        });
    }

    // CLIQUE DO MENU BOX
    function menuBox(){
        $(document).on("click",".er-menu__box",function(){
            $(this).toggleClass('er-menu-open');
            if($(this).hasClass('er-menu-open')){
                $(this).find('.er-menu__box-list').slideDown();
            }else{
                $(this).find('.er-menu__box-list').slideUp();
            }
        });
    };

    // ADD COMENTARIOS
    function add(btn,item){
        $(document).on("click",btn,function(){
            $(this).closest('.er-dragdrop__box').append(item);
        });
    }

    // FECHAR
    function close(){
        $(document).on("click","#er-box-delete",function(){
            gridster.remove_widget( $(this).closest('.er-dragdrop__item'));
            if(gridster.$widgets.length <= 0){
                $('#er-save').prop('disabled',true);
            } else {
                $('#er-save').prop('disabled',false);
            }
        });
    }

    return {
        init: init,
        menu: menuBox,
        add: add,
        close: close
    }

};
