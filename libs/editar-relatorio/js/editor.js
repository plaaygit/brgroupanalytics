/**
/* EDITOR
/* ----------------------------------------------------------------------------------------------------------------------------
/**/
var edit = function(){

    // INIT
    function init(){
        $('#er-editor-save').click(function(){
            $('.er-modal').css('visibility','hidden');
            $('.er-modal__item').hide();
            if($('#er-editor-btn-html').hasClass('active')){
                var edit = $('#er-editor-html').val();
            }else{
                var edit = $('#er-editor-text').html();
            }
            $(item).html(edit);
        });
        $('#er-editor-btn-bold').click(function(){
            document.execCommand('bold');
        });
        $('#er-editor-btn-italic').click(function(){
            document.execCommand('italic');
        });
        $('#er-editor-btn-underline').click(function(){
            document.execCommand('underline');
        });
        $('#er-editor-btn-link').click(function(){
            document.execCommand("CreateLink", false, "#");
        });
        $('#er-editor-btn-html').click(function(){
            $(this).toggleClass('active');
            if($(this).hasClass('active')){
                $(this).text("VISUALIZAR");
                $('#er-editor-html').val($('#er-editor-text').html());
                $('#er-editor-text').hide();
                $('#er-editor-html').show();
                $('#er-editor-btn-bold,#er-editor-btn-italic,#er-editor-btn-underline,#er-editor-btn-link').prop('disabled',true);
            }else {
                $(this).text("HTML");
                $('#er-editor-text').html($('#er-editor-html').val());
                $('#er-editor-html').hide();
                $('#er-editor-text').show();
                $('#er-editor-btn-bold,#er-editor-btn-italic,#er-editor-btn-underline,#er-editor-btn-link').prop('disabled',false);
            }
        });
    };

    // COMPONENTES EDITAVEIS
    function editable(edit){
        $(document).on("click",edit,function(){
            item = $(this);
            $('.er-modal').css('visibility','visible');
            $('.er-editor').show();
            $('#er-editor-text').html(item.html());
            $('#er-editor-html').val(item.html());
        });
        $(document).on("mouseenter",edit,function(){
            $(this).addClass('er-editor__on');
        });
        $(document).on("mouseleave",edit,function(){
            $(this).removeClass('er-editor__on');
        });
    };

    return {
        init: init,
        edit: editable,
    }

};
