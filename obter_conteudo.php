<?php
// bibliotecas
require_once 'apis/Instagram.php';
require_once 'apis/twitteroauth/twitteroauth.php';
require_once 'apis/twitteroauth/config.php';
use MetzWeb\Instagram\Instagram;

// get comentarios instagram
function get_posts_instagram($campanha, $username_owner, $post_id_instagram){
  $redesocial = 'instagram';
  $instagram = new Instagram('cdfe580286f54373a5f2b2061cce851d');
  $post_id = $instagram->getMediaShortcode($post_id_instagram);
  $username_owner = $post_id->data->user->username;
  $post_id = $post_id->data->id;
  $get_comments = $instagram->getMediaComments($post_id);
  foreach ($get_comments->data as $post) {
    $comentario = $post->text;
    $usuario = $post->from->username;
    $usuario_foto = $post->from->profile_picture;
    $comentario_id = $post->id;
    $created = strtotime($post->created_time);
    $likes = 0;
    add_comentario($redesocial, $username_owner, $post_id_instagram, $comentario_id, addslashes($comentario), $usuario, $usuario_foto, $likes, $campanha, $created);
  }
}

function get_posts_twitter($campanha, $username_owner, $post_id_twitter, $post_id_reply){
  $redesocial = 'twitter';
  $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
  $method = "statuses/show/".$post_id_reply;
  $post = $connection->get($method);
  $comentario = $post->text;
  $usuario = $post->user->screen_name;
  $usuario_foto = $post->user->profile_image_url_https;
  $comentario_id = $post->id_str;
  $created = strtotime($post->created_at);
  $likes = $post->favorite_count;
  add_comentario($redesocial, $username_owner, $post_id_twitter, $comentario_id, addslashes($comentario), $usuario, $usuario_foto, $likes, $campanha, $created);

}

// get comentarios facebook
function get_posts_fb($campanha, $username_owner, $post_id_facebook){
  $redesocial = 'facebook';
  $access_token = "CAABqw2P4APQBAOYvAwAnWWBdH3apg2zudZA5Lrv5JLR9gsKMJMrjXvxhyREEqmpG23ZAmUsAIdiHFCiYitii8S9jKp2hZCQ9T7i3N3l0ZCZCKsTbGXsBM7NalzRpS0uCg3N5ZCito6PulHzTXdYDZCJTH62sFDAu4hZCbbfUWL1VYrjbZCqX6W9BnqkgSn26CZC4BgeYpn5ZCEH8wZDZD";
  $graph_url_id = "https://graph.facebook.com/v2.5/{$username_owner}/?access_token=".$access_token;
  $user_id = json_decode(file_get_contents($graph_url_id))->id;
  $graph_url = "https://graph.facebook.com/v2.5/{$user_id}_{$post_id_facebook}/comments?limit=50&access_token=".$access_token;
  $i = 1;
  while($i != 0){
    $fb = file_get_contents($graph_url);
    $posts = json_decode($fb);
    foreach ($posts->data as $post){
      $comentario = $post->message;
      $usuario = $post->from->name;
      $usuario_foto = "https://graph.facebook.com/".$post->from->id."/picture";
      $comentario_id = $post->id;
      $likes = $post->like_count;
      $created = strtotime($post->created_time);
      add_comentario($redesocial, $username_owner, $post_id_facebook, $comentario_id, addslashes($comentario), addslashes($usuario), $usuario_foto, $likes, $campanha, $created);
    }
    if(isset($posts->paging->next)){
      $graph_url = $posts->paging->next;
      $i++;
      if($i > 2){
        $i = 0;
      }
    }else{
      $i=0;
    }
  }
}

function get_posts_youtube($campanha, $username_owner, $post_id_youtube){
  $redesocial = 'youtube';
  $access_token = "AIzaSyDK5TMPevSZmpMHRr8hrIgTaQcOrEMZXpM";
  $api_url = "https://www.googleapis.com/youtube/v3/commentThreads?part=snippet%2Creplies&videoId=".$post_id_youtube."&maxResults=100&key=".$access_token;
  $yt = file_get_contents($api_url);
  $yt = json_decode($yt);
  foreach ($yt->items as $post) {
    $comentario_id = $post->id;
    $post = $post->snippet->topLevelComment->snippet;
    $comentario = $post->textDisplay;
    $usuario = $post->authorDisplayName;
    $usuario_foto = $post->authorProfileImageUrl;
    $created = strtotime($post->publishedAt);
    $likes = 0;
    add_comentario($redesocial, $username_owner, $post_id_youtube, $comentario_id, addslashes($comentario), addslashes($usuario), $usuario_foto, $likes, $campanha, $created);
  }
}

// atualizar dados do post TO DO (separar funcoes em cada rede)
function get_posts_info($campanha){
  conectar();
  $sql = "SELECT * FROM posts WHERE id_campanha = '$campanha' ORDER BY id DESC";
  $sql = mysql_query ($sql);
  while ($l = mysql_fetch_array($sql)) {
    $rede = $l['redesocial'];
    $post_id = $l['post_id'];
    $username = $l['username'];
    //echo $rede.'<br>';
    get_post_info($post_id, $username, $rede);
  }
}

// update data posts
function get_post_info($post_id, $username, $rede){
  if($rede == "instagram"){
    $instagram = new Instagram('cdfe580286f54373a5f2b2061cce851d');
    $post = $instagram->getMediaShortcode($post_id);
    $user = $instagram->getUser($post->data->user->id);
    $in['followers'] = $user->data->counts->followed_by;
    $in['usuario_foto'] = $user->data->profile_picture;
    $in['likes'] = $post->data->likes->count;
    $in['comments'] = $post->data->comments->count;
    $in['foto'] = $post->data->images->standard_resolution->url;
    $in['legenda'] = addslashes($post->data->caption->text);
    $in['marcacoes'] = get_marcacoes($post_id, $username);
    $in['marcacoes'] = intval(($in['comments'] * $in['marcacoes']['porcentagem']) / 100);
    $in['created_time'] = $post->data->created_time;
    update_posts($rede, $post_id, $in);
  }
  if($rede == "twitter"){
    $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
    /* statuses/show */
    $method = "statuses/show/".$post_id;
    $data = $connection->get($method);
    $twitter = array();
    $twitter['legenda'] = $data->text;
    $twitter['foto'] = $data->user->profile_image_url;
    $twitter['followers'] = $data->user->followers_count;
    $twitter['rts'] = $data->retweet_count;
    $twitter['favs'] = $data->favorite_count;
    $twitter['created_time'] = strtotime($data->created_at);
    update_posts($rede, $post_id, $twitter);
  }
  if($rede == "facebook"){
    $access_token = urlencode("CAABqw2P4APQBAOYvAwAnWWBdH3apg2zudZA5Lrv5JLR9gsKMJMrjXvxhyREEqmpG23ZAmUsAIdiHFCiYitii8S9jKp2hZCQ9T7i3N3l0ZCZCKsTbGXsBM7NalzRpS0uCg3N5ZCito6PulHzTXdYDZCJTH62sFDAu4hZCbbfUWL1VYrjbZCqX6W9BnqkgSn26CZC4BgeYpn5ZCEH8wZDZD");
    $graph_url_id = "https://graph.facebook.com/v2.5/{$username}/?fields=about,access_token,engagement,name,talking_about_count,username,website&access_token=".$access_token;
    $fb_userdata = json_decode(file_get_contents($graph_url_id));
    $user_id = $fb_userdata->id;
    $graph_url = "https://graph.facebook.com/v2.5/{$user_id}_{$post_id}/?fields=shares,likes.limit(1).summary(true),comments.limit(1).summary(true),created_time,message,from,full_picture,link,object_id,picture,type&access_token=".$access_token;
    $fb = json_decode(file_get_contents($graph_url));
    if($fb->type == 'video'){
      $facebook['video'] = "https://www.facebook.com/video/embed?video_id=".$post_id;
    }else{
      $facebook['video'] = NULL;
    }
    $facebook['created_time'] = strtotime($fb->created_time);
    $facebook['followers'] = $fb_userdata->engagement->count;
    $facebook['usuario_foto'] = "http://graph.facebook.com/".$username."/picture";
    $facebook['legenda'] = $fb->message;
    $facebook['foto'] = $fb->full_picture;
    $facebook['likes'] = $fb->likes->summary->total_count;
    $facebook['shares'] = $fb->shares->count;
    $facebook['comments'] = $fb->comments->summary->total_count;
    $facebook['marcacoes'] = get_marcacoes($rede, $post_id, $username);
    $facebook['marcacoes'] = intval(($in['comments'] * $in['marcacoes']['porcentagem']) / 100);
    //echo "<br>".$graph_url."<br>";
    update_posts($rede, $post_id, $facebook);
  }
}
?>
