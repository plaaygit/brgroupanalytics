<?php
//error_reporting(0);
set_time_limit(0);
ignore_user_abort(true);
ini_set('max_execution_time', 0);
header('Content-Type: text/html; charset=UTF-8');
date_default_timezone_set('America/Sao_Paulo');
if(session_id() == '') {
  session_start();
}
// urls de paginas conteudo
$site_url = "http://relatorios.plaaymedia.com";
$page['home'][0] = $site_url."/";
$page['campanhas'][0] = $site_url."/campanhas";
$page['calendario'][0] = $site_url."/calendario";
$page['criar_campanha'][0] = $site_url."/campanhas/criar";
$page['configuracoes'][0] = $site_url."/configuracoes";
$page['ver_posts'][0] = $site_url."/campanhas/ver";
$page['editar_campanha'][0] = $site_url."/campanhas/editar";
$page['adicionar_post'][0] = $site_url."/campanhas/add-post";
$page['editar_post'][0] = $site_url."/campanhas/editar-post";
$page['como_nos_saimos'][0] =  $site_url."/campanhas/editar-relatorio";
// admin
$page['admin']['avaliar-posts'][0] = $site_url."/admin/avaliar-posts";
$page['admin']['usuarios'][0] = $site_url."/admin/usuarios";
$page['admin']['usuarios-criar'][0] = $site_url."/admin/criar-usuario";
$page['admin']['usuarios-editar'][0] = $site_url."/admin/editar-usuario";
$page['admin']['financeiro'][0] = $site_url."/admin/financeiro";
$page['relatorio'][0] = $site_url."/relatorios";
$page['relatorio'][1] = $site_url."/pages/relatorio";
if(!isset($role)){
  $role = 3;
}
// conectar ao banco de dados
function conectar(){
    $conexao = @mysql_connect("187.108.194.112", "root", "mplaay49r") or print (mysql_error());
    mysql_select_db("relatorios", $conexao) or print(mysql_error());
    mysql_query("charset = 'utf8mb4'");
    mysql_query("SET character_set_connection='utf8mb4'");
    mysql_query("set character_set_client = 'utf8mb4'");
    mysql_query("SET character_set_results='utf8mb4'");
}

// remover acentos
function str_case($term, $tp = 0) {
    if ($tp == "1") $palavra = strtr(strtoupper($term),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
    elseif ($tp == "0") $palavra = strtr(strtolower($term),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ");
    return $palavra;
}

function numero($n){
    return number_format($n,0,",",".");
}

function get_marcacoes($post_id, $username){
    conectar();
    $arroba = "SELECT * FROM comentarios WHERE post_id = '$post_id' AND comentario LIKE '%@%' AND comentario NOT LIKE '%@".$username."%'";
    $arroba = mysql_query ($arroba) or print_r(mysql_error());
    $arroba = mysql_num_rows($arroba);

    $total = "SELECT * FROM comentarios WHERE post_id = '$post_id'";
    $total = 	mysql_query ($total) or print_r(mysql_error());
    $total = mysql_num_rows($total);
    $calc = ($arroba * 100) / $total;
    $result = array('marcacoes' => $arroba, 'porcentagem' => $calc);
    return $result;
}

function limitarTexto($texto, $limite){
    $contador = strlen($texto);
    if ( $contador >= $limite ) {
        $texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '[...]';
        return $texto;
    }
    else{
        return $texto;
    }
}

function array_sort($array, $on, $order=SORT_ASC){

    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}

function slug($string,$space="-") {
    if (function_exists('iconv')) {
        $string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    }
    $string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
    $string = strtolower($string);
    $string = str_replace(" ", $space, $string);
    return $string;
}

?>
