<?php
// exibir dados da campanha no login
function get_campanhas_login($id){
	conectar();
	$sql = "SELECT * FROM campanhas WHERE id = $id";
	$sql = 	mysql_query ($sql);
	$campanhas = array();
	while($l = mysql_fetch_array($sql)){
		$campanhas['id'] = $l['id'];
		$campanhas['nome'] = $l['nome'];
		$campanhas['cliente'] = $l['cliente'];
		$campanhas['download'] = $l['download'];
		$campanhas['senha'] = $l['senha'];
		$campanhas['status'] = $l['status'];
		$campanhas['empresa'] = $l['empresa'];
		$campanhas['created'] = $l['created'];
		$campanhas['prazo'] = $l['prazo'];
	}
	return $campanhas;
}

// exibir campanhas e dado de campanha específica
function get_campanhas($id = false){
	conectar();
	if (!$id){
		if($_SESSION['role'] <= 1){
			$sql = "SELECT c.id, c.nome, c.cliente, c.download, c.status,c.senha, c.valor, c.fatura, c.created, c.prazo, e.empresa FROM campanhas as c INNER JOIN empresas as e on c.empresa = e.id ORDER BY id DESC";
			$ativas = "SELECT * FROM campanhas WHERE status='0'";
			$aguardando_entrega = "SELECT * FROM campanhas WHERE status='2'";
		}else{
			$sql = "SELECT * FROM campanhas WHERE empresa='".$_SESSION['empresa']."' ORDER BY id DESC";
			$ativas = "SELECT * FROM campanhas WHERE empresa='".$_SESSION['empresa']."' AND status='0'";
			$aguardando_entrega = "SELECT * FROM campanhas WHERE empresa='".$_SESSION['empresa']."' AND status='2'";
		}
		$sql = 	mysql_query($sql);
		$ativas = 	mysql_query($ativas);
		$ativas = mysql_num_rows($ativas);
		$aguardando_entrega = 	mysql_query($aguardando_entrega);
		$aguardando_entrega = mysql_num_rows($aguardando_entrega);
		$total = mysql_num_rows($sql);
		if($total == 0){
			$controller['error'] = 0;
			$controller['message'] = "Nenhuma campanha criada";
		}
		$i = 0;
		$campanhas = array();
		while($l = mysql_fetch_array($sql)){
			$campanhas['posts'][$i]['id'] = $l['id'];
			$campanhas['posts'][$i]['nome'] = $l['nome'];
			$campanhas['posts'][$i]['cliente'] = $l['cliente'];
			$campanhas['posts'][$i]['status'] = $l['status'];
			$campanhas['posts'][$i]['created'] = $l['created'];
			$campanhas['posts'][$i]['empresa'] = $l['empresa'];
			$campanhas['posts'][$i]['prazo'] = $l['prazo'];
			$i++;
		}
		$campanhas['data']['total'] = $total;
		$campanhas['data']['ativas'] = $ativas;
		$campanhas['data']['aguardando_entrega'] = $aguardando_entrega;
		return $campanhas;
	}else{
		if($_SESSION['role'] <= 1){
			$sql = "SELECT * FROM campanhas WHERE id = $id";
		}else{
			$sql = "SELECT * FROM campanhas WHERE empresa = '".$_SESSION['empresa']."' AND id = $id";
		}
		$sql = 	mysql_query ($sql);
		$campanhas = array();
		while($l = mysql_fetch_array($sql)){
			$campanhas['id'] = $l['id'];
			$campanhas['nome'] = $l['nome'];
			$campanhas['cliente'] = $l['cliente'];
			$campanhas['download'] = $l['download'];
			$campanhas['senha'] = $l['senha'];
			$campanhas['status'] = $l['status'];
			$campanhas['empresa'] = $l['empresa'];
			$campanhas['created'] = $l['created'];
			$campanhas['prazo'] = $l['prazo'];
		}
		return $campanhas;
	}
}

// lista de redes sociais em campanha
function get_redessocias($campanha){
	conectar();
	$sql = "SELECT DISTINCT(redesocial) FROM posts WHERE id_campanha =  $campanha";
	$sql = mysql_query ($sql)or print_r(mysql_error());
	$i = 0;
	while($l = mysql_fetch_array($sql)){
		$r[$l[0]] = true;
	}
	return $r;
}


// exibir posts das campanhas
function get_post_embed($post){
	conectar();
	$sql = "SELECT * FROM posts WHERE post_id = '$post'";
	$sql = 	mysql_query ($sql);
	$posts = array();
	while($l = mysql_fetch_array($sql)){
		$posts['id'] = $l['id'];
		$posts['influenciador'] = $l['influenciador'];
		$posts['username'] = $l['username'];
		$posts['usuario_foto'] = $l['usuario_foto'];
		$posts['post_id'] = $l['post_id'];
		$posts['foto'] = $l['foto'];
		$posts['legenda'] = $l['legenda'];
		$posts['redesocial'] = $l['redesocial'];
		$posts['data'] = $l['data'];
		$posts['campanha'] = $l['id_campanha'];
	}
	return $posts;
}
// exibir posts das campanhas
function get_posts($campanha, $post = false){
	conectar();
	if($post){
		$sql = "SELECT * FROM posts WHERE id = '$post'";
		$sql = 	mysql_query ($sql);
		if(mysql_num_rows($sql) == 0){
			$controller['error'] = 0;
			$controller['message'] = "Nenhuma post na campanha";
		}
		$posts = array();
		while($l = mysql_fetch_array($sql)){
			$posts['id'] = $l['id'];
			$posts['influenciador'] = $l['influenciador'];
			$posts['username'] = $l['username'];
			$posts['usuario_foto'] = $l['usuario_foto'];
			$posts['post_id'] = $l['post_id'];
			$posts['foto'] = $l['foto'];
			$posts['legenda'] = $l['legenda'];
			$posts['redesocial'] = $l['redesocial'];
			$posts['data'] = $l['data'];
			$posts['campanha'] = $l['id_campanha'];
			if($l['redesocial'] == "facebook"){
				$posts['post_id_link'] = 'http://facebook.com/'.$l['username'].'/posts/'.$l['post_id'];
				$posts['fb_views'] = $l['fb_views'];
			}
			if($l['redesocial'] == "instagram"){
				$posts['followers'] = $l['in_followers'];
				$posts['in_likes'] = $l['in_likes'];
				$posts['in_comments'] = $l['in_comments'];
				$posts['in_marcacoes'] = $l['in_marcacoes'];
				$posts['in_views'] = $l['in_views'];
				$posts['post_id_link'] = 'http://instagram.com/p/'.$l['post_id'];
			}
			if($l['redesocial'] == "twitter"){
				$posts['post_id_link'] = 'http://twitter.com/'.$l['username'].'/status/'.$l['post_id'];
			}
			if($l['redesocial'] == "youtube"){
				$posts['yt_inscritos'] = $l['yt_inscritos'];
				$posts['yt_totalviews'] = $l['yt_totalviews'];
				$posts['yt_views'] = $l['yt_views'];
				$posts['yt_comments'] = $l['yt_comments'];
				$posts['yt_likes'] = $l['yt_likes'];
				$posts['yt_dislikes'] =  $l['yt_dislikes'];
				$posts['post_id_link'] = 'https://www.youtube.com/watch?v='.$l['post_id'];
			}
			if($l['redesocial'] == "snapchat"){
				$posts['post_id_link'] = 'https://www.youtube.com/watch?v='.$l['post_id'];
				$posts['snap_views'] = $l['snap_views'];
				$posts['snap_prints'] = $l['snap_prints'];
			}

			return $posts;
		}
	}else{
		$sql = "SELECT * FROM posts WHERE id_campanha = '$campanha' ORDER BY influenciador ASC";
		$sql = 	mysql_query ($sql);
		if(mysql_num_rows($sql) == 0){
			global $controller;
			$controller['error'] = 3;
			$controller['message'] = "Nenhum post na campanha";
		}
		$i = 0;
		$posts = array();
		//$posts['total'] = mysql_num_rows($sql);
		while($l = mysql_fetch_array($sql)){
			$posts[$i]['id'] = $l['id'];
			$posts[$i]['influenciador'] = $l['influenciador'];
			$posts[$i]['username'] = $l['username'];
			$posts[$i]['usuario_foto'] = $l['usuario_foto'];
			$posts[$i]['post_id'] = $l['post_id'];
			$posts[$i]['redesocial'] = $l['redesocial'];
			$posts[$i]['campanha'] = $l['id_campanha'];
			$posts[$i]['data'] = $l['data'];
			if($l['redesocial'] == "facebook"){
				$posts[$i]['fb_likes'] = $l['fb_likes'];
				$posts[$i]['fb_comments'] = $l['fb_comments'];
				$posts[$i]['fb_shares'] = $l['fb_shares'];
				$posts[$i]['engajamento'] = $l['fb_likes'] + $l['fb_comments'] + $l['fb_shares'] + $l['fb_views'];
				$posts[$i]['followers'] = $l['fb_pagelikes'];
				$posts[$i]['fb_marcacoes'] = $l['fb	_marcacoes'];
				$posts[$i]['fb_views'] = $l['fb_views'];
				$posts[$i]['post_id_link'] = 'http://facebook.com/'.$l['username'].'/posts/'.$l['post_id'];
			}
			if($l['redesocial'] == "instagram"){
				$posts[$i]['in_likes'] = $l['in_likes'];
				$posts[$i]['in_comments'] = $l['in_comments'];
				$posts[$i]['engajamento'] = $l['in_likes'] + $l['in_comments'] + $l['in_views'];
				$posts[$i]['followers'] = $l['in_followers'];
				$posts[$i]['in_marcacoes'] = $l['in_marcacoes'];
				$posts[$i]['in_views'] = $l['in_views'];
				$posts[$i]['post_id_link'] = 'http://instagram.com/p/'.$l['post_id'];
			}
			if($l['redesocial'] == "twitter"){
				$posts[$i]['tw_rts'] = $l['tw_rts'];
				$posts[$i]['tw_favs'] = $l['tw_favs'];
				$posts[$i]['tw_replies'] = $l['tw_replies'];
				$posts[$i]['engajamento'] = $l['tw_rts'] + $l['tw_replies'] + $l['tw_favs'];
				$posts[$i]['followers'] = $l['tw_followers'];
				$posts[$i]['post_id_link'] = 'http://twitter.com/'.$l['username'].'/status/'.$l['post_id'];
			}
			if($l['redesocial'] == "youtube"){
				$posts[$i]['followers'] = $l['yt_inscritos'];
				$posts[$i]['yt_inscritos'] = $l['yt_inscritos'];
				$posts[$i]['yt_totalviews'] = $l['yt_totalviews'];
				$posts[$i]['yt_views'] = $l['yt_views'];
				$posts[$i]['yt_comments'] = $l['yt_comments'];
				$posts[$i]['yt_likes'] = $l['yt_likes'];
				$posts[$i]['yt_dislikes'] =  $l['yt_dislikes'];
				$posts[$i]['engajamento'] = $l['yt_views'] + $l['yt_comments'] + ($l['yt_likes'] - $l['yt_dislikes']);
				$posts[$i]['post_id_link'] = 'https://www.youtube.com/watch?v='.$l['post_id'];
			}
			if($l['redesocial'] == "snapchat"){
				$posts[$i]['post_id_link'] = 'https://www.youtube.com/watch?v='.$l['post_id'];
				$posts[$i]['snap_views'] = $l['snap_views'];
				$posts[$i]['snap_prints'] = $l['snap_prints'];
				$posts[$i]['engajamento'] = $l['snap_views'] + $l['snap_prints'] ;
			}

			$i++;
		}
	}
	return $posts;
}
// get engajamento
function get_engajamento($campanha){
	$posts = get_posts($campanha);
	$r = array();
	foreach ($posts as $post) {
		if($post['redesocial'] == 'instagram'){
			$r['instagram']['geral'] += $post['in_likes'] + $post['in_comments'] + $post['in_views'];
			$r['instagram']['likes'] += $post['in_likes'];
			$r['instagram']['comments'] += $post['in_comments'];
			$r['instagram']['marcacoes'] += $post['in_marcacoes'];
			$r['instagram']['views'] += $post['in_views'];
			$r['instagram']['posts']++;
		}
		if($post['redesocial'] == 'facebook'){
			$r['facebook']['geral'] += $post['fb_likes'] + $post['fb_comments'] + $post['fb_shares'] + $post['fb_views'];
			$r['facebook']['shares'] += $post['fb_shares'];
			$r['facebook']['views'] += $post['fb_views'];
			$r['facebook']['likes'] += $post['fb_likes'];
			$r['facebook']['comments'] += $post['fb_comments'];
			$r['facebook']['posts']++;
		}
		if($post['redesocial'] == 'twitter'){
			$r['twitter']['geral'] += $post['tw_rts'] + $post['tw_replies'] + $post['tw_favs'];
			$r['twitter']['rts'] += $post['tw_rts'];
			$r['twitter']['favs'] += $post['tw_favs'];
			$r['twitter']['replies'] += $post['tw_replies'];
			$r['twitter']['posts']++;
		}
		if($post['redesocial'] == 'youtube'){
			$r['youtube']['geral'] += $post['yt_views'] + $post['yt_comments'] + ($post['yt_likes'] - $post['yt_dislikes']);
			$r['youtube']['views'] += $post['yt_views'];
			$r['youtube']['likes'] += $post['yt_likes'];
			$r['youtube']['dislikes'] += $post['yt_dislikes'];
			$r['youtube']['comments'] += $post['yt_comments'];
			$r['youtube']['posts']++;
		}
		if($post['redesocial'] == 'snapchat'){
			$r['snapchat']['geral'] += $post['snap_views'] + $post['snap_prints'];
			$r['snapchat']['views'] += $post['snap_views'];
			$r['snapchat']['prints'] += $post['snap_prints'];
			$r['snapchat']['posts']++;		}
		}
		return $r;
	}


	// get comentarios infp
	function get_comentarios_info($campanha){
		$comments_total = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE campanha = '$campanha'"));
		$comments_positivo = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE campanha = '$campanha' AND sentimento = '1'"));
		$comments_neutro = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE campanha = '$campanha' AND sentimento = '2'"));
		$comments_negativo = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE campanha = '$campanha' AND sentimento = '3'"));
		$comentarios_info = array();
		$comentarios_info['positivos'] = ($comments_positivo * 100) / $comments_total;
		$comentarios_info['neutros'] = ($comments_neutro * 100) / $comments_total;
		$comentarios_info['negativos'] = ($comments_negativo * 100) / $comments_total;
		return $comentarios_info;
	}

	// get comentários destaque
	function get_comentario_destaque($campanha, $post_id = false){
		conectar();
		if($post_id){
			$sql = "SELECT * FROM comentarios WHERE campanha = '$campanha' AND post_id ='$post_id' AND sentimento = '1' AND destaque ='1'";
			$sql = mysql_query ($sql)or print_r(mysql_error());
			$comments_total = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE post_id = '$post_id'"));
			$comments_positivo = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE post_id ='$post_id' AND sentimento = '1'"));
			$comments_neutro = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE post_id = '$post_id' AND sentimento = '2'"));
			$comments_negativo = mysql_num_rows(mysql_query("SELECT * FROM comentarios WHERE post_id = '$post_id' AND sentimento = '3'"));
			$i=0;
			$return = array();
			if(mysql_num_rows($sql) != 0){
				while ($l = mysql_fetch_array($sql)) {
					$return['sentimento']['positivo'] = round(($comments_positivo * 100) / $comments_total);
					$return['sentimento']['neutro'] = round(($comments_neutro * 100) / $comments_total);
					$return['sentimento']['negativo'] = round(($comments_negativo * 100) / $comments_total);
					$return['comentarios_destaque'][$i]['usuario'] = $l['usuario'];
					if($l['redesocial'] == "instagram" || $l['redesocial'] == "twitter" ){
						$return['comentarios_destaque'][$i]['usuario_foto'] = "http://apiavatar.plaaymedia.com/".$l['redesocial']."/".$l['usuario']."";
					}else{
					$return['comentarios_destaque'][$i]['usuario_foto'] = $l['usuario_foto'];
					}
					$return['comentarios_destaque'][$i]['comentario'] = $l['comentario'];
					$i++;
				}
				return $return;
			}
			if(mysql_num_rows($sql) == 0){
				$return['sentimento']['positivo'] = round(($comments_positivo * 100) / $comments_total);
				$return['sentimento']['neutro'] = round(($comments_neutro * 100) / $comments_total);
				$return['sentimento']['negativo'] = round(($comments_negativo * 100) / $comments_total);
				return $return;
			}
		}else{
			$sql = "SELECT * FROM comentarios WHERE campanha = '$campanha' AND destaque ='1'";
			$sql = mysql_query ($sql)or print_r(mysql_error());
			$i=0;
			if(mysql_num_rows($sql) == 0){
				return false;
			}else{
				while ($l = mysql_fetch_array($sql)) {
					$sentimento = $l['sentimento'];
					if($sentimento == 1){ $sentimento = 'positivo'; }
					if($sentimento == 2){ $sentimento = 'neutro'; }
					if($sentimento == 3){ $sentimento = 'negativo';}
					$return[$sentimento][$i]['usuario'] = $l['usuario'];
					if($l['redesocial'] == "instagram" || $l['redesocial'] == "twitter" ){
						$return[$sentimento][$i]['usuario_foto'] = "http://apiavatar.plaaymedia.com/".$l['redesocial']."/".$l['usuario']."";
					}else{
					$return[$sentimento][$i]['usuario_foto'] = $l['usuario_foto'];
					}
					$return[$sentimento][$i]['comentario'] = $l['comentario'];
					$i++;
				}
				return $return;

			}
		}
	}

	// exibir posts relatorio
	function gerar_relatorio($campanha){
		conectar();
		$sql = "SELECT * FROM posts WHERE id_campanha = $campanha ORDER BY influenciador ASC, data";
		$sql = mysql_query ($sql)or print_r(mysql_error());
		$i = 0;
		while ($l = mysql_fetch_array($sql)) {
			$rede = $l['redesocial'];
			$influenciador = $l['influenciador'];
			$username = $l['username'];
			if($l['redesocial'] == "twitter" || $l['redesocial'] == "instagram"){
				$usuario_foto = "http://apiavatar.plaaymedia.com/".$l['redesocial']."/".$l['username']."";
			}else{
				$usuario_foto = $l['usuario_foto'];
			}
			$id_post = $l['post_id'];
			$data =  date('d/m/Y', strtotime($l['data']));
			if($rede == "instagram"){
				$return[$i][$rede]['influenciador'] = $influenciador;
				$return[$i][$rede]['username'] = $username;
				$return[$i][$rede]['usuario_foto'] = $usuario_foto;
				$return[$i][$rede]['foto'] = $l['foto'];
				$return[$i][$rede]['legenda'] = $l['legenda'];
				$return[$i][$rede]['post_id'] = $id_post;
				$return[$i][$rede]['data'] = $data;
				$return[$i][$rede]['in_followers'] = $l['in_followers'];
				$return[$i][$rede]['in_likes'] = $l['in_likes'];
				$return[$i][$rede]['in_comments'] = $l['in_comments'];
				$return[$i][$rede]['in_marcacoes'] = $l['in_marcacoes'];
				$return[$i][$rede]['in_views'] = $l['in_views'];
				$return[$i][$rede]['interacoes'] = $return[$i][$rede]['in_likes'] + $return[$i][$rede]['in_comments'] + $return[$i][$rede]['in_views'];
				$return[$i][$rede]['comentarios'] = get_comentario_destaque($campanha, $id_post);
			}
			if($rede == "twitter"){
				$return[$i][$rede]['influenciador'] = $influenciador;
				$return[$i][$rede]['username'] = $username;
				$return[$i][$rede]['usuario_foto'] = $usuario_foto;
				$return[$i][$rede]['legenda'] = $l['legenda'];
				$return[$i][$rede]['data'] = $data;
				$return[$i][$rede]['post_id'] = $id_post;
				$return[$i][$rede]['tw_followers'] = $l['tw_followers'];
				$return[$i][$rede]['tw_rts'] = $l['tw_rts'];
				$return[$i][$rede]['tw_favs'] = $l['tw_favs'];
				$return[$i][$rede]['tw_replies'] = $l['tw_replies'];
				$return[$i][$rede]['interacoes'] = $return[$i][$rede]['tw_rts'] + $return[$i][$rede]['tw_favs'];
				$return[$i][$rede]['comentarios'] = get_comentario_destaque($campanha, $id_post);
			}
			if($rede == "facebook"){
				$return[$i][$rede]['influenciador'] = $influenciador;
				$return[$i][$rede]['username'] = $username;
				$return[$i][$rede]['usuario_foto'] = $usuario_foto;
				$return[$i][$rede]['foto'] = $l['foto'];
				if($l['video']){
				$return[$i][$rede]['video'] = $l['video'];
				}
				$return[$i][$rede]['legenda'] = $l['legenda'];
				$return[$i][$rede]['data'] = $data;
				$return[$i][$rede]['post_id'] = $id_post;
				$return[$i][$rede]['fb_pagelikes'] = $l['fb_pagelikes'];
				$return[$i][$rede]['fb_likes'] = $l['fb_likes'];
				$return[$i][$rede]['fb_shares'] = $l['fb_shares'];
				$return[$i][$rede]['fb_comments'] = $l['fb_comments'];
				$return[$i][$rede]['fb_views'] = $l['fb_views'];
				$return[$i][$rede]['interacoes'] = $return[$i][$rede]['fb_likes'] + $return[$i][$rede]['fb_comments'] + $return[$i][$rede]['fb_shares'] + $return[$i][$rede]['fb_views'];;
				$return[$i][$rede]['comentarios'] = get_comentario_destaque($campanha, $id_post);
			}

			if($rede == "youtube"){
				$return[$i][$rede]['influenciador'] = $influenciador;
				$return[$i][$rede]['username'] = $username;
				$return[$i][$rede]['usuario_foto'] = $usuario_foto;
				$return[$i][$rede]['data'] = $data;
				$return[$i][$rede]['post_id'] = $id_post;
				$return[$i][$rede]['yt_inscritos'] = $l['yt_inscritos'];
				$return[$i][$rede]['yt_totalviews'] = $l['yt_totalviews'];
				$return[$i][$rede]['yt_views'] = $l['yt_views'];
				$return[$i][$rede]['yt_likes'] = $l['yt_likes'];
				$return[$i][$rede]['yt_dislikes'] = $l['yt_dislikes'];
				$return[$i][$rede]['yt_comments'] = $l['yt_comments'];
				$return[$i][$rede]['interacoes'] = $return[$i][$rede]['yt_views'] + $return[$i][$rede]['yt_comments'] + ($return[$i][$rede]['yt_likes'] - $return[$i][$rede]['yt_dislikes']);
				$return[$i][$rede]['comentarios'] = get_comentario_destaque($campanha, $id_post);
				$return[$i][$rede]['youtube'] = get_youtube_data($campanha, $id_post);
			}

			if($rede == "snapchat"){
				$return[$i][$rede]['influenciador'] = $influenciador;
				$return[$i][$rede]['username'] = $username;
				$return[$i][$rede]['usuario_foto'] = $usuario_foto;
				$return[$i][$rede]['data'] = $data;
				$return[$i][$rede]['post_id'] = $id_post;
				$return[$i][$rede]['snap_views'] = $l['snap_views'];
				$return[$i][$rede]['snap_prints'] = $l['snap_prints'];
				$return[$i][$rede]['interacoes'] = $return[$i][$rede]['snap_views'] + $return[$i][$rede]['snap_prints'];
			}
			$i++;
		}
		mysql_close();
		return $return;
	}
	// get comentarios

	function get_comentario_avaliar($numero, $outro1 = NULL, $outro2 = NULL){
		conectar();
		if(isset($outro1) && isset($outro2)){
			$sql = "SELECT * FROM comentarios WHERE sentimento = 0 AND id != {$outro1} AND id != {$outro2} ORDER BY campanha ASC LIMIT {$numero}";
		}else{
			if(isset($outro1) && !isset($outro2)){
				$sql = "SELECT * FROM comentarios WHERE sentimento = 0 AND id != {$outro1} AND id != {$outro2} ORDER BY campanha ASC LIMIT {$numero}";
			}else{
				$sql = "SELECT * FROM comentarios WHERE sentimento = 0 ORDER BY campanha ASC LIMIT {$numero}";
			}
		}
		$comentario = array();
		$sql = 	mysql_query ($sql);
		if(mysql_num_rows($sql) == 0){
			$comentario[0]['usuario_foto'] = "http://lorempixel.com/80/80/cats/";
			$comentario[0]['comentario'] = "Não temos mais comentários para exibir :(";
			$comentario[0]['created'] = "ops";
			$comentario[0]['id'] = "error";
			$comentario[0]['redesocial'] = "error";
		}
		$i = 0;
		while($l = mysql_fetch_array($sql)){
			$comentario[$i]['usuario'] = $l['usuario'];
			$comentario[$i]['usuario_foto'] = $l['usuario_foto'];
			$comentario[$i]['comentario'] = $l['comentario'];
			$comentario[$i]['created'] = $l['created'];
			$comentario[$i]['id'] = $l['id'];
			$comentario[$i]['post_id'] = $l['post_id'];
			$comentario[$i]['redesocial'] = $l['redesocial'];
			$comentario[$i]['username_owner'] = $l['username_owner'];
			$comentario[$i]['campanha'] = $l['campanha'];
			$i++;
		}
		return $comentario;
	}

	function get_campanhas_notes($campanha){
		conectar();
		$sql = "SELECT * FROM campanhas_notes WHERE campanha = $campanha";
		$sql = 	mysql_query ($sql);
		while($l = mysql_fetch_array($sql)){
			$r['sexo_f'] = $l['sexo_f'];
			$r['sexo_m'] = $l['sexo_m'];
			$r['como_nos_saimos'] = $l['como_nos_saimos'];
			$r['destaques'] = json_decode($l['destaques'], true);

		}
		return $r;
	}

	// get youtube data
	function get_youtube_data($campanha, $post_id){
		conectar();
		$sql = "SELECT * FROM youtube_data WHERE campanha = '$campanha' AND post_id ='$post_id'";
		$sql = mysql_query ($sql)or print_r(mysql_error());

		while ($l = mysql_fetch_array($sql)) {
			$return['sexo']['sexo_m'] = $l['sexo_m'];
			$return['sexo']['sexo_f'] = $l['sexo_f'];
			$return['assistido'] = json_decode($l['assistido'], true);
			$return['data'] = json_decode($l['data'], true);
			$i++;
		}
		return $return;

	}

	// exibir usuários
	function get_users($usuario = false){
		conectar();
		if($post){
			$sql = "SELECT * FROM usuarios WHERE id = '$usuario'";
			$sql = 	mysql_query ($sql);
			if(mysql_num_rows($sql) == 0){
				$controller['error'] = 0;
				$controller['message'] = "Nenhum usuário encontrado";
			}
			$usuario = array();
			while($l = mysql_fetch_array($sql)){
				$usuario['id'] = $l['id'];
				$usuario['usuario'] = $l['usuario'];
				$usuario['role'] = $l['role'];
				$usuario['empresa'] = $l['empresa'];
				$usuario['empresa-nome'] = $l['empresa_nome'];
				$usuario['empresa-logo'] = $l['empresa_logo'];
				return $usuario;
			}
		}else{
			$sql = "SELECT * FROM usuarios";
			$sql = 	mysql_query ($sql);
			if(mysql_num_rows($sql) == 0){
				global $controller;
				$controller['error'] = 3;
				$controller['message'] = "Nenhum usuário encontrado";
			}
			$i = 0;
			$usuarios = array();
			//$posts['total'] = mysql_num_rows($sql);
			while($l = mysql_fetch_array($sql)){
				$usuarios[$i]['id'] = $l['id'];
				$usuarios[$i]['usuario'] = $l['usuario'];
				$usuarios[$i]['role'] = $l['role'];
				$usuarios[$i]['empresa'] = $l['empresa'];
				$usuarios[$i]['empresa-nome'] = $l['empresa_nome'];
				$usuarios[$i]['empresa-logo'] = $l['empresa_logo'];
				$i++;
			}
		}
		return $usuarios;
	}

	// get faturas
	function get_faturas($id = false){
		conectar();
		if (!$id){
			$sql = "SELECT * FROM faturas ORDER BY id DESC";
			$sql = 	mysql_query($sql);
			// $ativas = "SELECT * FROM campanhas WHERE status='0'";
			// $ativas = 	mysql_query($ativas);
			// $ativas = mysql_num_rows($ativas);
			// $aguardando_entrega = "SELECT * FROM campanhas WHERE status='2'";
			// $aguardando_entrega = 	mysql_query($	aguardando_entrega);
			// $aguardando_entrega = mysql_num_rows($aguardando_entrega);
			$total = mysql_num_rows($sql);
			if($total == 0){
				global $controller;
				$controller['error'] = 3;
				$controller['message'] = "Nenhuma fatura disponível.";
			}
			$i = 0;
			$faturas = array();
			while($l = mysql_fetch_array($sql)){
				$faturas['faturas'][$i]['id'] = $l['id'];
				$faturas['faturas'][$i]['nome'] = $l['nome'];
				$faturas['faturas'][$i]['cliente'] = $l['cliente'];
				$faturas['faturas'][$i]['status'] = $l['status'];
				$i++;
			}
			$faturas['data']['total'] = $total;
			// $faturas['data']['ativas'] = $ativas;
			// $faturas['data']['aguardando_entrega'] = $aguardando_entrega;
			return $faturas;
		}else{
			$sql = "SELECT * FROM campanhas WHERE id = $id";
			$sql = 	mysql_query ($sql);
			$campanhas = array();
			while($l = mysql_fetch_array($sql)){
				$campanhas['id'] = $l['id'];
				$campanhas['nome'] = $l['nome'];
				$campanhas['cliente'] = $l['cliente'];
				$campanhas['download'] = $l['download'];
				$campanhas['senha'] = $l['senha'];
				$campanhas['status'] = $l['status'];
				$campanhas['created'] = $l['created'];
				$campanhas['prazo'] = $l['prazo'];
			}
			return $campanhas;
		}
	}

	?>
