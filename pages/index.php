<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['home']['status'] = true;
include_once("template/header.php");
$dados = get_campanhas();
$total = $dados['data']['total'];
$ativas = $dados['data']['ativas'];
$aguardando_entrega = $dados['data']['aguardando_entrega'];
?>
<!-- box home -->
<div class="row">
  <div class="col-xs-12 col-sm-4 col-md-4">
    <div class="box amarelobg">
      <a href="http://relatorios.plaaymedia.com/campanhas/?q=aguardando+entrega">
        <div class="data"><?php echo $aguardando_entrega;?></div>
        <div class="subtitle">aguardando entrega</div>
      </a>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4 col-md-4">
    <div class="box verdebg">
      <a href="http://relatorios.plaaymedia.com/campanhas/?q=ativo">
        <div class="data"><?php echo $ativas;?></div>
        <div class="subtitle">campanhas ativas</div>
      </a>
    </div>
  </div>
  <div class="col-xs-12 col-sm-4 col-md-4">
    <div class="box azulbg">
      <a href="http://relatorios.plaaymedia.com/campanhas/">
        <div class="data"><?php echo $total;?></div>
        <div class="subtitle">campanhas totais</div>
      </a>
    </div>
  </div>
</div>
<!-- /box home -->
<!-- ultimas campanhas -->
<div class="row row-pm">
  <h1 class="titulo">Últimas campanhas <span><a href="<?php echo $page['criar_campanha'][0];?>">criar nova</a></span></h1>
  <div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
      <thead>
        <tr>
          <th>Nome da campanha</th>
          <th class="hidden-xs">Cliente</th>
          <th>Status</th>
          <th class="hidden-sm hidden-xs">Data de criação</th>
          <th class="hidden-sm hidden-xs">Prazo</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 0;
        foreach ($dados['posts'] as $campanha) {
          if($campanha['status'] == 0){$campanha['status'] = "<span style='color:#3498db'>ativo</span>";}
          if($campanha['status'] == 1){$campanha['status'] = "<span style='color:#2ecc71'>entregue</span>";}
          if($campanha['status'] == 2){$campanha['status'] = "<span style='color:#ECBE04'>aguardando entrega</span>";}
          echo '<tr class="odd gradeX">
          <td><a href="'.$page['campanhas'][0].'/ver/'.$campanha['id'].'">'.$campanha['nome'].'</a></td>
          <td class="hidden-xs">'.$campanha['cliente'].'</td>
          <td>'.$campanha['status'].'</td>
          <td class="hidden-sm hidden-xs" data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['created']).'">'.date('d/m/Y', strtotime($campanha['created'])).'</td>
          <td class="hidden-sm hidden-xs" data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['prazo']).'">'.date('d/m/Y', strtotime($campanha['prazo'])).'</td>
          </tr>';
          $i++;
          if($i >= 8){
            break;
          }
        }
        ?>
      </tbody>
    </table>
  </div>
  <!-- /.table-responsive -->

</div>
<!-- /ultimas campanhas -->
<?php
$footer = <<<EOF
EOF;
include_once("template/footer.php");
?>
