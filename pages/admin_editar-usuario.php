<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
include_once("../alterar_conteudo.php");
$page['admin']['usuarios']['status'] = true;
$post = get_posts(0, $_GET['id']);
$info_campanha = get_campanhas($post['campanha']);
if(isset($_POST['influenciador'])){
  $influenciador = $_POST['influenciador'];
  $username = $_POST['username'];
  $post_id =  $_POST['post_id'];
  $redesocial =  $_POST['redesocial'];
  if($redesocial == 'facebook'){
    $fb_views = $_POST['fb_views'];
  }else{
    $fb_views = 0;
  }
  $post =  editar_post($influenciador, $username, $post_id, $fb_views, $redesocial, $_GET['id']);
  $controller['error'] = 1;
  $controller['message'] = "Post editado com sucesso! <a href='".$site_url."/campanha/ver/".$campanha."'>Ver campanha.</a>";
}
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Editar usuário</h1>
  <form role="form" id="form" method="post">
    <div class="form-group">
      <label>Usuário</label>
      <input class="form-control" name="influenciador" value="<?php echo $post['influenciador'];?>" required>
      <p class="help-block">Nome do influenciador.</p>
    </div>
    <div class="form-group">
      <label>Senha</label>
      <input class="form-control" name="username" value="<?php echo $post['username'];?>" required>
      <p class="help-block">Username do influenciador.</p>
    </div>
    <div class="form-group">
      <label>Role</label>
      <input class="form-control" name="post_id" type="text" value="<?php echo $post['post_id'];?>" required>
      <p class="help-block">Id do post.</p>
    </div>
    <div class="form-group">
      <label  for="radios">Rede Social</label>
      <select id="selectbasic" name="redesocial" class="form-control" required>
        <option value="">Escolha a rede social do post</option>
        <?php
        $redes = array('twitter','facebook','instagram','youtube');

        foreach($redes as $rede){
          $selected = '' ;
          if($post['redesocial']==$rede) $selected='selected';
          echo '<option value="'.$rede.'" '.$selected.'>'.ucfirst($rede).'</option>';
        }?>
      </select>
    </div>
    <button type="submit" class="btn btn-primary ">Editar post</button>
    <button type="reset" class="btn btn-danger" data-toggle="modal" data-target="#deletar_influenciador">Deletar post</button>
  </form>
</div>
<?php
$footer = <<<EOF
  <div id="deletar_influenciador" class="modal fade" role="dialog">
  <div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal">&times;</button>
  <h4 class="modal-title">DELETAR CAMPANHA</h4>
  </div>
  <div class="modal-body">
  <center>
  <p>Você tem certeza que deseja deletar essa post?</p><br>
  <p>Essa ação é definitiva e irreversível.</p>
  </center>
  </div>
  <div class="modal-footer">
  <center>
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
  <a class="btn btn-danger btn_deletar_conta">Deletar</a>
  </center>
  </div>
  </div>

  </div>
  </div>
EOF;
include_once("template/footer.php");
?>
