<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['ver_posts']['status'] = true;
if(!is_numeric($_GET['id'])){
  header("Location: ".$page['campanhas'][0]);
  die;
}
$info_campanha = get_campanhas($_GET['id']);
if($info_campanha == NULL){
  header("Location: ".$page['campanhas'][0]);
  die;
}

$dados = get_posts($_GET['id']);
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Posts na campanha <strong><?php echo $info_campanha['nome'];?></strong> <span><a href="<?php echo $page['adicionar_post'][0].'/'.$info_campanha['id'];?>">adicionar post</a></span>

      <input class="checkbox" id="checkbox1" <?php if($info_campanha['status'] == 2) echo 'checked';?> type="checkbox"/>
      <?php if($info_campanha['status'] == 1){
        echo '<span class="ver-relatorio"><span><a href="'.$page['home'][0].'relatorios/'.slug($info_campanha['cliente']).'/'.$info_campanha['id'].'"><i class="fa fa-pie-chart"></i> ver relatório</a></span></span>';
      }
      else{?>
<label for="checkbox1" class="checkbox-label">
  <span class="fim">Finalizado:</span>
  <span class="on"></span>
  <span class="off"></span>
</label>
<?php }?></h1>
  <div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
      <thead>
        <tr>
          <th>Influenciador</th>
          <th class="hidden-xs">Username</th>
          <th>Rede Social</th>
          <th class="hidden-xs">Data</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dados as $post) {
          if(isset($post['id'])){
            echo '<tr class="odd gradeX">
            <td><a href="'.$page['editar_post'][0].'/'.$post['id'].'">'.$post['influenciador'].'</a></td>
            <td class="hidden-xs">'.$post['username'].'</td>
            <td><a href="'.$post['post_id_link'].'" target="_blank">'.ucfirst($post['redesocial']).'</a></td>
            <td class="hidden-xs" data-search="'.strtotime($post['data']).'" data-order="'.strtotime($post['data']).'">'.date('d/m/Y', strtotime($post['data'])).'</td>
            </tr>';
          }
        }

        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
$footer = <<<EOF
<div id="fechar_relatorio" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Fechar campanha</h4>
</div>
<div class="modal-body">
<center>
<p>Escolha uma data para a entrega do relatório <span style="font-size: 15px;font-weight: 300;">(necessita aprovação)</span></p><br>
<div id="prazo"></div>
<div id="hora" style="display:none; width: 300px"></div></center>
</div>
<div class="modal-footer">
<center>
<a class="btn btn-primary btn_fechar_relatorio" data-idcampanha="{$_GET['id']}" >Fechar campanha</a>
</center>
</div>
</div>

</div>
</div>

<script>

$(function() {
  $("#prazo").datepicker({ dateFormat: "yy-mm-dd", minDate: 1, maxDate: "+14D", defaultDate: null, setDate: null, onSelect: function(dateText) { $('#prazo').hide(); $('#hora').show(); } } );
  $('#prazo').find(".ui-state-active").removeClass("ui-state-active"); // this actually removes the highlight
  $('#prazo').find(".ui-state-hover").removeClass("ui-state-hover"); // this actually removes the highlight
  $('#hora').datetimepicker({ inline: true, format: 'LT',stepping:15, enabledHours: [10, 11, 12, 13, 14, 15, 16, 17, 18, 19],
 });
});

$(document).ready(function() {
  $('#tabela_campanha').DataTable({
    responsive: true,
    "order": [[ 3, 'desc' ]],
    language: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });
});
var id_campanha = {$_GET['id']};
</script>
EOF;
include_once("template/footer.php");
?>
