<?php
include_once("../../functions.php");
include_once("../../exibir_conteudo.php");
$numero = $_GET['data'];
if(isset($_GET['outro1']) && isset($_GET['outro2'])){
  $comentario = get_comentario_avaliar($numero,$_GET['outro1'], $_GET['outro2']);
}else{
  $comentario = get_comentario_avaliar($numero);
}
$n = 0;
$numero = $numero - 1;
while ($n <= $numero) {
$info_campanha = get_campanhas($comentario[$n]['campanha']);
$campanha = $info_campanha['nome'];
$cliente = $info_campanha['cliente'];
$json[0] = $comentario[$n]['redesocial'];
$json[1] = $comentario[$n]['username_owner'];
$json[2] = $comentario[$n]['post_id'];
$post = get_post_embed($comentario[$n]['post_id']);
$img = $post['foto'];
$legenda = $post['legenda'];
$url = $site_url."/pages/admin_embed.php?id=".json_encode($json);
if($json[0] == "instagram"){
$embed = <<<EOF
<a href="http://instagram.com/p/{$json[2]}" target="_blank">
<img src="{$img}" width="100%" height="500px" style="width:100%; border-radius:0;"/>
</a>
{$legenda}
EOF;
}else{
$embed = <<<EOF
<iframe src='{$url}' width="100%" height="700" frameborder="0"/></iframe>
EOF;
}
echo <<<EOF
<section id="{$comentario[$n]['id']}" class="cover">
<div class="col-xs-12 col-md-7">
<div class="box-dado-main">

<ul class="box-dado-chat">
<li class="chat-item">
<figure class="chat-image">
<img src="{$comentario[$n]['usuario_foto']}" height="100" width="100" alt="avatar" />
<figcaption class="chat-message">
<p>{$comentario[$n]['comentario']}</p>
<span class="chat-time">~ {$comentario[$n]['created']}</span>
</figcaption>
</figure>
</li>
</ul>
</div>
<div class="row avalie">
<div class="center">
<a href="javascript:avaliar_comentario({$comentario[$n]['id']}, 1);">
<button type="button" class="btn btn-info btn-circle btn-lg"><i class="fa fa-thumbs-o-up"></i></button>
</a>
<a href="javascript:avaliar_comentario({$comentario[$n]['id']}, 2);">
<button type="button" class="btn btn-warning btn-circle btn-lg"><i class="fa fa-adjust"></i></button>
</a>
<a href="javascript:avaliar_comentario({$comentario[$n]['id']}, 3);">
<button type="button" class="btn btn-danger btn-circle btn-lg"><i class="fa fa-thumbs-o-down"></i>
</button>
</a>
</div>
</div>
</div>
<div class="col-xs-12 col-md-5">
<h4 style="padding-left:0px; font-weight:100"><b>{$campanha}</b> - {$cliente}</h4>
{$embed}
</div>
</section>
EOF;
$n++;
}
  ?>
