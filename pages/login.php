<?php
include_once "../functions.php";
include_once "../exibir_conteudo.php";
include_once "../login-painel.php";
if($_GET['permissao'] == 'false'){
  $controller = array();
  $controller['error'] = 2;
  $controller['message'] = "Você não tem permissão para acessar essa página.";
}
if(isset($_GET['sair'])){
  session_start();
  session_destroy();
}
?>
<!doctype html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<title>Login</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<!-- build:css(.) styles/vendor.css -->
	<!-- bower:css -->
	<link href="http://relatorios.plaaymedia.com/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- endbower -->
	<!-- endbuild -->
	<!-- build:css(.tmp) styles/main.css -->
	<style>
	.wrapper{

    background: rgba(255,93,56,1);
    background: -moz-linear-gradient(top, rgba(255,93,56,1) 0%, rgba(228, 80, 80,1) 100%);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgb100%,93,56,1)), color-stop(100%, rgba(228, 80, 801)));
    background: -webkit-linear-gradient(top, rgba(255,93,56,1) 0%, rgba(228, 80, 80,1) 100%);
    background: -o-linear-gradient(top, rgba(255,93,56,1) 0%, rgba(228, 80, 80,1) 100%);
    background: -ms-linear-gradient(top, rgba(255,93,56,1) 0%, rgba(228, 80, 80,1) 100%);
    background: linear-gradient(to bottom, rgba(255,93,56,1) 0%, rgba(228, 80, 80,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5d38', endColorstr='#ff2965', GradientType=0 );
		height: 100%;
		width: 100%;
		display: block;
		position: absolute;
		top: 0;
		left: 0;
	}


	.login {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		margin: auto;
		max-width: 400px;
		/*height: 332px;*/
		overflow: hidden;
		display: inline-table;
		/*background: #ffffff;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		-ms-border-radius: 5px;
		border-radius: 5px;
		-webkit-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
		-moz-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
		-ms-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
		box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);*/
		z-index: 5;
	}

	.login .content {
		padding: 40px 60px;
	}

	.login .content .logo-plaaymedia {
		text-align: center;
		width: 100%;
		padding-bottom: 32px;
	}

	.login .content .login-form {
		padding: 0 10px;
	}

	.login .content .login-form fieldset {
		border: 2px solid #f2f2f2;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		-ms-border-radius: 5px;
		border-radius: 5px;
	}

	.login .content .login-form label {
		width: 100%;
		margin-bottom: 0;
	}

	.login .content .login-form input{
		padding: 15px;
		width: 100%;
		border: none;
		border-top: 2px solid #f2f2f2;
		margin-top: -2px;
		font-size: 16px;
		outline: none;
		font-weight: normal;
		height: 55px;
		background: #fff;
		-webkit-appearance:none;
		-moz-appearance:none;
		appearance:none;
	}
  input:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px white inset;
}

	.login .content .login-form select {
		padding: 15px;
		width: 100%;
		border: none;
		border-top: 2px solid #f2f2f2;
		margin-top: -2px;
		font-size: 16px;
		outline: none;
		font-weight: normal;
		height: 55px;
		background: #fff;
		-webkit-appearance:none;
		-moz-appearance:none;
		appearance:none;

		color:#A8A9A9;
	}
	.no-br{
		border-radius: 0;
	}
	.login .content .btn {
		width: 100%;
		padding: 10px;
		margin-top: 10px;
		font-size: 16px;
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		-ms-border-radius: 4px;
		border-radius: 4px;
	}

	.alert {
		margin-bottom: -15px !important;
	}
  .btn-enviar {
    color: #fff;
    background-color: rgba(0, 0, 0, 0.1);
    border-color: rgba(0, 0, 0, 0.05);
  }
  .btn-enviar:hover {
    color: #fff;
    background-color: rgba(0, 0, 0, 0.2);
    border-color: rgba(0, 0, 0, 0.08);
}
.btn-enviar:focus{
  outline: none!important;
  color:#fff;,1
}
	</style>
	<link href="http://relatorios.plaaymedia.com/css/animate.css" rel="stylesheet" type="text/css">
	<!-- endbuild -->
	<!-- build:js scripts/vendor/modernizr.js -->

</head>
<body>
	<!--[if lt IE 10]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<div class="wrapper">
		<div class="login animated <?php if (!isset($controller['message'])) { echo("bounceInDown"); } ?>">
			<?php require_once("template/erros.php"); ?>
			<div class="content">
				<figure class="logo-plaaymedia" >
					<img src="http://relatorios.plaaymedia.com/img/logo.png" width="250" />
				</figure>
				<form class="login-form" action="#" method="post">
					<fieldset>
						<label>
							<input name="usuario" type="text" autocomplete="off" required placeholder="Usuário"/>
						</label>
						<label>
							<input name="senha" id="senha" type="password" required placeholder="Senha" readonly onfocus="this.removeAttribute('readonly');"/>
						</label>
					</fieldset>
					<button type="submit" class="btn btn-enviar">Entrar</button>
				</form>
			</div>
		</div>

		<div class="push"></div>
	</div>
  <script src="<?php echo $site_url;?>/js/jquery.min.js"></script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-37490571-9', 'auto');
	ga('send', 'pageview');

if($(window).width() < 768){
  $('#senha').removeAttr('readonly');
}
	</script>
</body>
</html>
