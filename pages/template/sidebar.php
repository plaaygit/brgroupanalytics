<div id="conteudo">
  <!-- sidebar -->
  <div id="sidebar">
    <ul>
      <li class="titulo">Painel</li>
      <li><a href="<?php echo $page['home'][0]; ?>" <?php echo isset($page['home']['status']) ? 'class="atual"' : ''; ?>>Home</a></li>
      <li><a href="<?php echo $page['campanhas'][0]; ?>" <?php echo isset($page['campanhas']['status']) ? 'class="atual"' : ''; ?>>Campanhas</a></li>
      <li><a href="<?php echo $page['calendario'][0]; ?>" <?php echo isset($page['calendario']['status']) ? 'class="atual"' : ''; ?>>Calendário</a></li>
      <li><a href="<?php echo $page['criar_campanha'][0]; ?>" <?php echo isset($page['criar_campanha']['status']) ? 'class="atual"' : ''; ?>>Criar Campanha</a></li>
      <?php if(isset($info_campanha)){
        ?>
        <li class="titulo separacao"><?php echo $info_campanha['nome']; ?></li>
        <li><a href="<?php echo $page['ver_posts'][0]."/".$info_campanha['id']; ?>" <?php echo isset($page['ver_posts']['status']) ? 'class="atual"' : ''; ?>>Ver posts</a></li>
        <li><a href="<?php echo $page['editar_campanha'][0]."/".$info_campanha['id']; ?>" <?php echo isset($page['editar_campanha']['status']) ? 'class="atual"' : ''; ?>>Editar Campanha</a></li>
        <li><a href="<?php echo $page['adicionar_post'][0]."/".$info_campanha['id']; ?>" <?php echo isset($page['adicionar_post']['status']) ? 'class="atual"' : ''; ?>>Adicionar post</a></li>
        <li><a href="<?php echo $page['como_nos_saimos'][0]."/".$info_campanha['id']; ?>" <?php echo isset($page['como_nos_saimos']['status']) ? 'class="atual"' : ''; ?>>Como nos saímos</a></li>

        <?php
        if(isset($page['editar_post']['status'])){
          ?>
        <li><a href="#" <?php echo isset($page['editar_post']['status']) ? 'class="atual"' : '' ;?>>Editar post</a></li>
          <?php
        }
      }?>
      <?php if($_SESSION['role'] <= 1){
        ?>
        <li class="titulo separacao">Admin</li>
        <li><a href="<?php echo $page['admin']['avaliar-posts'][0]; ?>" <?php echo isset($page['admin']['avaliar-posts']['status']) ? 'class="atual"' : ''; ?>>Avaliar posts</a></li>
        <?php if($_SESSION['role'] == 0){?>
          <li><a href="<?php echo $page['admin']['usuarios'][0]; ?>" <?php echo isset($page['admin']['usuarios']['status']) ? 'class="atual"' : ''; ?>>Usuários</a></li>
          <li><a href="<?php echo $page['admin']['financeiro'][0]; ?>" <?php echo isset($page['admin']['financeiro']['status']) ? 'class="atual"' : ''; ?>>Financeiro</a></li>
        <?php
        }
        if(isset($page['editar_post']['status'])){
          ?>
        <li><a href="#" <?php echo isset($page['editar_post']['status']) ? 'class="atual"' : ''; ?>>Editar post</a></li>
          <?php
        }
      }?>
      <li class="titulo separacao">Conta</li>
      <li><a href="<?php echo $page['configuracoes'][0]; ?>">Configurações</a></li>
      <li><a href="<?php echo $page['home'][0]; ?>login/?sair">Sair</a></li>

    </ul>
  </div>
  <!-- /sidebar -->
<?php
  include_once("content.php");
?>
