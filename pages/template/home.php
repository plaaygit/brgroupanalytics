<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Relatórios</title>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="css/normalize.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,cr-1.2.0,fc-3.1.0,fh-3.0.0,kt-2.0.0,r-1.0.7,rr-1.0.0,sc-1.3.0,se-1.0.1/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,cr-1.2.0,fc-3.1.0,fh-3.0.0,kt-2.0.0,r-1.0.7,rr-1.0.0,sc-1.3.0,se-1.0.1/datatables.min.js"></script>
</head>
<body>
  <!-- heaader -->
  <header class="navbar navbar-fixed-top">
    <div id="hamburger">
      <i class="fa fa-bars"></i>
    </div>
    <div id="logo">
      <img src="img/logo.png">
    </div>
  </header>
  <!-- /heaader -->
  <div id="conteudo">
    <!-- sidebar -->
    <div id="sidebar">
      <ul>
        <li class="titulo">Painel</li>
        <li><a href="" class="selected">Home</a></li>
        <li><a href="">Campanhas</a></li>
        <li><a href="">Criar Campanha</a></li>
        <li class="final"><a href="">Configurações</a></li>

        <li class="titulo">Mastercard Surpreenda</li>
        <li><a href="">Ver posts</a></li>
        <li><a href="">Editar Campanha</a></li>
        <li><a href="">Adicionar posts</a></li>
      </ul>
    </div>
    <!-- /sidebar -->
    <!-- conteudo display -->
    <div id="display">
      <div class="container-fluid conteudo-padding">
        <!-- box home -->
        <div class="row">
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="box">
              <div class="data">200</div>
              <div class="subtitle">aguardando entrega</div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="box">
              <div class="data">200</div>
              <div class="subtitle">campanhas ativas</div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4">
            <div class="box">
              <div class="data">200</div>
              <div class="subtitle">campanhas totais</div>
            </div>
          </div>
        </div>
        <!-- /box home -->
        <!-- ultimas campanhas -->
        <div class="row row-pm">
          <h1 class="titulo">Últimas campanhas <span><a href="nova_campanha.php">criar nova</a></span></h1>
          <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
              <thead>
                <tr>
                  <th>Nome da campanha</th>
                  <th>Cliente</th>
                  <th>Status</th>
                  <th>Data de criação</th>
                  <th>Prazo</th>
                </tr>
              </thead>
              <tbody>
                <tr class="odd gradeX">
                  <td><a href="posts.php?campanha='.$campanha['id'].'">'.$campanha['nome'].'</a></td>
                  <td>'.$campanha['cliente'].'</td>
                  <td>'.$campanha['status'].'</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['created']).'">aaaa</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['prazo']).'">aaaa</td>
                </tr>
                <tr class="odd gradeX">
                  <td><a href="posts.php?campanha='.$campanha['id'].'">'.$campanha['nome'].'</a></td>
                  <td>'.$campanha['cliente'].'</td>
                  <td>'.$campanha['status'].'</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['created']).'">aaaa</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['prazo']).'">aaaa</td>
                </tr>
                <tr class="odd gradeX">
                  <td><a href="posts.php?campanha='.$campanha['id'].'">'.$campanha['nome'].'</a></td>
                  <td>'.$campanha['cliente'].'</td>
                  <td>'.$campanha['status'].'</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['created']).'">aaaa</td>
                  <td data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['prazo']).'">aaaa</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.table-responsive -->

        </div>
        <!-- /ultimas campanhas -->
      </div>
      <!-- /conteudo display -->
    </div>
  </div>

</body>
</html
