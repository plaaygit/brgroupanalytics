<?php
//Coloring the box appropriately
if (isset($controller['error'])) {
      if ($controller['error'] == 0) {
        $class = "alert-info";
      }

      if ($controller['error'] == 2) {
        $class = "alert-danger";
      }

      if($controller['error'] == 1){
        $class="alert-success";
      }

      if ($controller['error'] == 3) {
        $class = "alert-warning";
      }
}
  if (isset($controller['message'])) {
?>
    <div class="alert <?php echo($class); ?> alert-dismissible animated fadeInDown" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?php echo($controller['message']) ?>
    </div>
<?php
  }
?>
