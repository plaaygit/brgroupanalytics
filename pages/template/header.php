<?php
if(session_id() == '') {
  session_start();
}

if (!isset($_SESSION['usuario-id'])) {
  $_SESSION['old-url'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  if($_SESSION['old-url'] == $site_url."/login/"){
    $_SESSION['old-url'] = $site_url;
  }
  header("Location: ".$site_url."/login/"); //HTTP 1.1
  die();
}
if(isset($_SESSION['role'])){
  if($_SESSION['role'] > $role){
    $_SESSION['old-url'] = $site_url;
    header("Location: ".$site_url."/login/?permissao=false"); //HTTP 1.1
    die();
  }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8">
  <title>Plataforma de relatórios - Plaay Media</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <link href="<?php echo $site_url;?>/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo $site_url;?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $site_url;?>/css/normalize.css" rel="stylesheet">
  <link href="<?php echo $site_url;?>/css/animate.css" rel="stylesheet">
  <link href="<?php echo $site_url;?>/css/style.css" rel="stylesheet">
  <link href="<?php echo $site_url;?>/libs/jquery-ui/jquery-ui.min.css" rel="stylesheet">
  <link href='<?php echo $site_url;?>/libs/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
  <link href='<?php echo $site_url;?>/libs/fullcalendar/fullcalendar.css' rel='stylesheet' />
  <link href='<?php echo $site_url;?>/css/bootstrap-datetimepicker.css' rel='stylesheet' />

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/bs/jq-2.1.4,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,cr-1.2.0,fc-3.1.0,fh-3.0.0,kt-2.0.0,r-1.0.7,rr-1.0.0,sc-1.3.0,se-1.0.1/datatables.min.css"/>
</head>
<body>
  <!-- heaader -->
  <header class="navbar navbar-fixed-top">
    <div id="hamburger" class="hidden-md hidden-lg">
      <i class="fa fa-bars"></i>
    </div>
    <div id="logo">
      <img src="<?php echo $site_url;?>/img/logo.png">
    </div>
  </header>
  <!-- /heaader -->
  <?php
  include_once("sidebar.php");
  ?>
