<!-- SNAPCHAT -->
<!-- ############################################################################################################### -->
<article class="theme--snapchat" id="Snapchat">

    <!-- Social snapchat -->
    <section class="slide" >

        <div class="social-headline">
            <div class="social-headline__background">
                <img class="social-headline__logo" src="<?php echo $page['relatorio'][1];?>/images/icon_snapchat.png" alt="snapchat" width="95" height="95" />
            </div>
            <div class="social-headline__counter">
                <div class="social-headline__counter-title">ENGAJAMENTO</div>
                <span class="social-headline__counter-number"><?php echo numero($engajamento['snapchat']['geral']);?></span>
            </div>
        </div>

        <div class="container container--ishead-social">
            <div class="container--fullflex">

                <div class="graph">
                    <div id="snapchatChart" class="graph__item"></div>
                    <div id="snapchatChartText" class="graph__text"></div>
                </div>

                <div class="slide__caption">
                    <p>Número de Posts</p>
                    <strong><?php echo $engajamento['snapchat']['posts'];?></strong>
                </div>

            </div>
        </div>
    </section>
    <script>
        //Snapchat Chart
        //----------------------------------------------------------------------
        var snapchatChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'snapchatChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            $('#snapchatChartTotal').text(arraySum(this.series[0].processedYData));
                            responsiveText(this,'#FFFFFF',true);
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:50,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#333;"><p>{point.name}</p><p>{point.y}</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Views',
                            y: <?php echo $engajamento['snapchat']['views'];?>,
                            color: '#FFFC50'
                        },
                        {
                            name: 'Screenshots',
                            y: <?php echo $engajamento['snapchat']['prints'];?>,
                            color: '#FFCB0C'
                        }
                    ],
                    name: ' '
                }]
            },

                                             function(chart) {
                var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
                var textY = chart.plotTop  + (chart.plotHeight * 0.5);

                var innerText = '<div class="graph-label__inner graph-label__inner-social graph-label__inner-snapchat" >';
                innerText += '<p>Total</p>';
                innerText += '<p id="snapchatChartTotal"></p>';
                innerText += '</div>';

                $("#snapchatChartText").append(innerText);
                innerText = $('.graph-label__inner');
            });
        };
    </script>

    <!-- Datatable snapchat -->
    <?php
    $count_Posts = 0;
    foreach ($start as $post) {
        if(isset($post['snapchat']['influenciador'])){
            $count_Posts++;
        }
    }
    foreach ($start as $key => $value) {
        if (key($value) == "snapchat") {
            $posts_snapchat[] = $value['snapchat'];
        }
    }

    $currentPost = 0;
    $divisor = ceil($count_Posts / 12) - 1;
    for($ab = 0; $ab <= $divisor; $ab++){
    ?>
    <section class="slide">
        <div class="container container--full">

            <div class="datatable">
                <table class="datatable__main">
                    <thead class="datatable__head">
                        <tr>
                            <th class="datatable__head-item datatable__left">Nome</th>
                            <th class="datatable__head-item datatable__center">#</th>
                            <th class="datatable__head-item">Data</th>
                            <th class="datatable__head-item">Views</th>
                            <th class="datatable__head-item">Screeshots</th>
                            <th class="datatable__head-item">Int. Totais</th>
                        </tr>
                    </thead>
                    <tbody class="datatable__body">
                        <?php
        for($contadorX = $currentPost; $contadorX <= $currentPost+12; $contadorX++){
            $porcentagem = ($posts_snapchat[$contadorX]['engajamento'] * 100) / $posts_snapchat[$contadorX]['followers'];
            if(isset($posts_snapchat[$contadorX]['influenciador'])){
                        ?>
                        <tr>
                            <td class="datatable__body-item datatable__left"><?php echo $posts_snapchat[$contadorX]['influenciador'];?></td>
                            <td class="datatable__body-item datatable__center">01</td>
                            <td class="datatable__body-item"><?php echo $posts_snapchat[$contadorX]['data'];?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_snapchat[$contadorX]['snap_views']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_snapchat[$contadorX]['snap_prints']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_snapchat[$contadorX]['interacoes']);?></td>
                        </tr>
                        <?
            }
        }
        $currentPost += 13;
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
    <?php
    }
    ?>

    <!-- Social snapchat details -->
    <?php
    $count_posts_snapchat = 0;
    foreach ($start as $post) {
        if(isset($post['snapchat'])){
            $post = $post['snapchat'];
            if(isset($post['influenciador'])){
                $count_posts_snapchat++;
                $porcentagem = ($post['interacoes'] * 100) / $post['in_followers'];
                if($porcentagem >= 0.5){
                    $tipo_engajamento = 'Regular';
                }
                if($porcentagem >= 1){
                    $tipo_engajamento = 'Médio';
                }
                if($porcentagem >= 1.5){
                    $tipo_engajamento = 'Alto';
                }
                if($porcentagem >= 2.5){
                    $tipo_engajamento = 'Muito alto';
                }
    ?>
    <section class="slide slide--social">

        <div class="social-headline__details">
            <div class="social-headline__details-step"><?php echo $count_posts_snapchat;?></div>
        </div>

        <div class="container container--ishead-details">
            <div class="container--fullflex">

                <div class="user">
                    <figure class="user__figure">
                        <img class="circle__image" src="<?php echo $post['usuario_foto'];?>" alt="<?php echo $post['influenciador'];?>" width="80" height="80" />
                    </figure>
                    <div class="user__details">
                        <h3 class="user__name"><?php echo $post['influenciador'];?></h3>
                        <p class="user__text">Engajamento não se aplica</p>
                    </div>
                </div>

                <ul class="social-list">
                    <?php if($post['post_id'] != '0'){ ?>
                    <li class="social-list__item social-list__item--right">
                        <div class="social-post">
                            <div class="social-post__item">
                                <iframe class="social-post__item-media" src="https://www.youtube.com/embed/<?php echo $post['post_id'];?>?ps=docs&showinfo=0&modestbranding=0&controls=0&rel=0&fs=1&hd=1&VQ=HD720" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </li>
                    <?php } ?>
                    <li class="social-list__item social-list__item--left">
                        <ul class="social-post__option">
                            <li class="social-post__option-item">
                                <h3>Views</h3>
                                <p><?php echo numero($post['snap_views']);?></p>
                            </li>
                            <li class="social-post__option-item">
                                <h3>Screenshots</h3>
                                <p><?php echo numero($post['snap_prints']);?></p>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </section>
    <?php
            }
        }
    }
    ?>

</article>
