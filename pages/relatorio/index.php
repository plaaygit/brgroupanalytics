<?php
session_start();
include_once "../../functions.php";
include_once "../../exibir_conteudo.php";
include_once "../../functions.php";
$bypass = @explode("?bypass=",$_SERVER['REQUEST_URI']);
if($bypass[1] == '39@'){
}else{
    if (!isset($_SESSION['campanha-id'])) {
        header("Location: ".$page['relatorio'][0]."/login/".$_GET['id']."/"); //HTTP 1.1
        die();
    }else{
        if($_SESSION['campanha-id'] != $_GET['id']){
            session_destroy();
            header("Location: ".$page['relatorio'][0]."/login/".$_GET['id']."/"); //HTTP 1.1
            die();
        }
    }
}

?>
<!DOCTYPE html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Relatórios de campanhas - Plaay Media</title>
        <link rel='shortcut icon' type='image/x-icon' href='/favicon.ico' />
        <link rel="stylesheet" href="<?php echo $page['relatorio'][1];?>/styles/styles.css">
        <script src="<?php echo $page['relatorio'][1];?>/scripts/vendor/modernizr.js"></script>
        <script src="<?php echo $page['relatorio'][1];?>/scripts/vendor/plugins.js"></script>
        <script src="<?php echo $page['relatorio'][1];?>/scripts/vendor/highchart.js"></script>
        <script src="<?php echo $page['relatorio'][1];?>/scripts/vendor/gridster.js"></script>
        <script src="<?php echo $page['relatorio'][1];?>/scripts/vendor/scrollify.js"></script>
        <script src="<?php echo $site_url;?>/js/moment.js"></script>
        <script src="<?php echo $site_url;?>/js/twemoji.min.js"></script>
    </head>

    <body>

        <!-- loading -->
        <div class="loading-campaign">
            <div>
                <img class="loading-image" src="<?php echo $page['relatorio'][1];?>/images/logo.png" alt="plaaymedia" >
                <div class="loading-spinner">
                    <div class="loading-spinner__inner">
                        <label>●</label>
                        <label>●</label>
                        <label>●</label>
                        <label>●</label>
                        <label>●</label>
                        <label>●</label>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $redes_include = get_redessocias($_GET['id']);
        $start = gerar_relatorio($_GET['id']);
        //print_r(json_encode($start));
        $engajamento = get_engajamento($_GET['id']);
        include_once "sumario.php";
        if(isset($redes_include['snapchat'])){
            include_once "snapchat.php";
        }
        if(isset($redes_include['facebook'])){
            include_once "facebook.php";
        }
        if(isset($redes_include['twitter'])){
            include_once "twitter.php";
        }
        if(isset($redes_include['instagram'])){
            include_once "instagram.php";
        }
        if(isset($redes_include['youtube'])){
            include_once "youtube.php";
        }
        include_once "blog.php";
        ?>

        <script src="<?php echo $page['relatorio'][1];?>/scripts/graphs.js"></script>
        <script src="<?php echo $page['relatorio'][1];?>/scripts/main.js"></script>

        <script>
            //Call all Charts
            //----------------------------------------------------------------------
            window.onload = function(){
                mainSocialChart();
                mainComparisonChart();
                mainEngagementChart();
                <?php
                if(isset($redes_include['facebook'])){
                ?>
                facebookChart();
                <?php
                }
                if(isset($redes_include['twitter'])){
                ?>
                twitterChart();
                <?php
                }
                if(isset($redes_include['instagram'])){
                ?>
                instagramChart();
                <?php
                }
                if(isset($redes_include['snapchat'])){
                ?>
                snapchatChart();
                <?php
                }
                if(isset($redes_include['youtube'])){
                ?>
                youtubeOneChart();
                youtubeTwoChart();
                <?php
                }
                ?>
                blogOneChart();
            };
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-10533035-18', 'auto');
            ga('send', 'pageview');

            if($(window).width() < 768){
                $('#senha').removeAttr('readonly');
            }
        </script>

    </body>
</html>
