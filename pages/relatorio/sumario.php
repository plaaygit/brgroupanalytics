<?php
$dados_campanha = get_campanhas($_GET['id']);
$posts = get_posts($_GET['id']);
$comentario_destaque = get_comentario_destaque($_GET['id']);
$destaques_campanha = get_campanhas_notes($_GET['id']);
$comentarios_info = get_comentarios_info($_GET['id']);
?>

<!-- HEADER -->
<!-- ############################################################################################################### -->
<header class="slide">

    <!-- Button Download PDF -->
    <?php
    if(isset($dados_campanha['download'])){
    ?>
    <a class="header__button" href="<?php echo $dados_campanha['download'];?>" target="_blank" title="Baixar PDF" >
        <i class="fa fa-cloud-download fa-2x"></i>
    </a>
    <?php
    }
    ?>

    <!-- Brand Campaign -->
    <div class="header-brand">
        <div class="container container--center">
            <img src="<?php echo $page['home'][0];?>/img/logo-emr-<?php echo $dados_campanha['empresa'];?>.png" width="345" height="47" alt="Brand" />
        </div>
    </div>

    <!-- Campaign Headline -->
    <div class="header-headline">
        <div class="container container--center">
            <h1>Engajamento de campanha <?php echo $dados_campanha['nome'];?></h1>
            <h2><?php echo $dados_campanha['cliente'];?></h2>
        </div>
    </div>

    <!-- Whitedoor Logo -->
    <div class="header-logo">
        <div class="container container--center">
            <img class="header-logo__image" src="<?php echo $page['relatorio'][1];?>/images/icon_whitedoor.png" width="75" height="75" alt="Whitedoor" />
            <div class="header-logo__name">
                <p>Whitedoor</p>
                <span><?php echo date("Y") ?></span>
            </div>
        </div>
    </div>

</header>

<!-- MAIN SOCIAL -->
<!-- ############################################################################################################### -->
<section class="slide slide__background--darkblue" id="sumario">

    <!-- headline -->
    <div class="slide-headline ">
        <div class="container container--center">
            <p>Engajamento total<i class="tooltips tooltips--dark" alt="Soma das interações dispníveis nas redes. No slide único das redes consta os dados utilizados."></i> separado por rede social.</p>
        </div>
    </div>

    <div class="container container--ishead" >
        <div class="container--fullflex">

            <!-- Graph -->
            <div class="graph">
                <div id="mainSocialChart" class="graph__item"></div>
                <div id="mainSocialChartText"></div>
            </div>

            <!-- Posts -->
            <div class="slide__caption">
                <p>Número de Posts</p>
                <strong><?php echo count($posts);?></strong>
            </div>

        </div>
    </div>

</section>

<script>
    //Main Social Chart
    //----------------------------------------------------------------------
    var mainSocialChart = function() {

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'mainSocialChart',
                type: 'pie',
                backgroundColor: null,
                events:{
                    load:function(){
                        $('#mainSocialChartTotal').text(arraySum(this.series[0].processedYData));
                        responsiveText(this,'#FFFFFF', true);
                        socialClick();
                        rotateAngle(this);
                    }
                }
            },
            title: {
                text: null
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: '60%',
                    startAngle: 45,
                    size: '210px',
                    borderWidth: 0,
                    dataLabels: {
                        connectorWidth: 3,
                        enabled: true,
                        useHTML: true,
                        distance:50,
                        softConnector: false,
                        connectorPadding:0,
                        formatter: function() {
                            return ('<div class="graph-label" style="border: 3px solid '+this.color+';"><p>'+this.point.name+' <img class="icon_graph" src="http://relatorios.plaaymedia.com/pages/relatorio/images/icon_'+this.point.name.replace(/[{( )}0-9]/g, '')+'.png" alt="'+this.point.name.replace(/[{( )}0-9]/g, '')+'" width="22"></p><p>'+this.y +'</p></div>');
                        },
                        style: {
                            textShadow: false,
                        }
                    }
                }
            },
            series: [{
                data: [
                    <?php
                    if(isset($engajamento['instagram'])){
                    ?>
                    {
                        name: 'Instagram (<?php echo $engajamento['instagram']['posts'];?>)',
                        y: <?php echo $engajamento['instagram']['geral'];?>,
                        color: '#3f729b'
                    },
                    <?
                    }
                    ?>

                    <?php
                    if(isset($engajamento['youtube'])){
                    ?>
                    {
                        name: 'Youtube (<?php echo $engajamento['youtube']['posts'];?>)',
                        y: <?php echo $engajamento['youtube']['geral'];?>,
                        color: '#cd201f'
                    },
                    <?
                    }
                    ?>
                    <?php
                    if(isset($engajamento['facebook'])){
                    ?>
                    {
                        name: 'Facebook (<?php echo $engajamento['facebook']['posts'];?>)',
                        y: <?php echo $engajamento['facebook']['geral'];?>,
                        color: '#3b5998'
                    },
                    <?
                    }
                    ?>
                    <?php
                    if(isset($engajamento['twitter'])){
                    ?>
                    {
                        name: 'Twitter (<?php echo $engajamento['twitter']['posts'];?>)',
                        y: <?php echo $engajamento['twitter']['geral'];?>,
                        color: '#00aeeb'
                    },
                    <?
                    }
                    ?>
                    <?php
                    if(isset($engajamento['snapchat'])){
                    ?>
                    {
                        name: 'Snapchat (<?php echo $engajamento['snapchat']['posts'];?>)',
                        y: <?php echo $engajamento['snapchat']['geral'];?>,
                        color: '#FFCB0C'
                    },
                    <?
                    }
                    ?>
                ],
                name: ' '
            }]
        },

                                         function(chart) {
            var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
            var textY = chart.plotTop  + (chart.plotHeight * 0.5);

            var innerText = '<div class="graph-label__inner" >';
            innerText += '<p>Total</p>';
            innerText += '<p id="mainSocialChartTotal"></p>';
            innerText += '</div>';

            $("#mainSocialChartText").append(innerText);
            innerText = $('.graph-label__inner');
        });
    };
</script>

<!-- MAIN COMPARISON -->
<!-- ############################################################################################################### -->
<section class="slide slide__background--softblue" id="comparacao">

    <!-- headline -->
    <div class="slide-headline">
        <div class="container container--center">
            <h3>COMPARAÇÃO</h3>
            <p>Destaques da campanha, gênero do público</p>
        </div>
    </div>

    <div class="container container--ishead">
        <ul class="container__three">
            <li class="container__three-item">

                <!-- Campaign's Features -->
                <ul class="featured">
                    <?php
                    $destaque_engajament = 0;
                    foreach ($destaques_campanha['destaques'] as $destaque) {
                        $destaque_engajament++;
                    ?>
                    <li class="featured__item">
                        <figure class="featured__image">
                            <img src="<?php echo $destaque['usuario_foto'];?>" width="80" height="80" alt="<?php echo $destaque['usuario'];?>" />
                            <span class="featured__position"><?php echo $destaque_engajament;?></span>
                        </figure>
                        <div class="featured__details">
                            <h3 class="featured__name"><?php echo $destaque['influenciador'];?></h3>
                            <p class="featured__text"><?php echo $destaque['destaque'];?></p>
                        </div>
                    </li>
                    <?php
                    }
                    ?>
                </ul>

            </li>
            <li class="container__three-item">

                <!-- Graph -->
                <div id="mainComparisonChart" class="graph"></div>

            </li>
            <li class="container__three-item" style="display:none">

                <!-- Top List 5 by region -->
                <div class="toplist toplist-5">
                    <div class="toplist__left">
                        <div class="toplist__title">Top 5</div>
                    </div>
                    <ul class="toplist__right">
                        <li class="toplist__item">
                            <div class="toplist__position">1</div>
                            <div class="toplist__details">
                                <p class="toplist__name">São Paulo</p>
                                <span class="toplist__text">50% de engajamento</span>
                            </div>
                        </li>
                        <li class="toplist__item">
                            <div class="toplist__position">2</div>
                            <div class="toplist__details">
                                <p class="toplist__name">Rio de Janeiro</p>
                                <span class="toplist__text">30% de engajamento</span>
                            </div>
                        </li>
                        <li class="toplist__item">
                            <div class="toplist__position">3</div>
                            <div class="toplist__details">
                                <p class="toplist__name">Paraná</p>
                                <span class="toplist__text">10% de engajamento</span>
                            </div>
                        </li>
                        <li class="toplist__item">
                            <div class="toplist__position">4</div>
                            <div class="toplist__details">
                                <p class="toplist__name">Rio Grande do Sul</p>
                                <span class="toplist__text">5% de engajamento</span>
                            </div>
                        </li>
                        <li class="toplist__item">
                            <div class="toplist__position">5</div>
                            <div class="toplist__details">
                                <p class="toplist__name">Acre</p>
                                <span class="toplist__text">5% de engajamento</span>
                            </div>
                        </li>
                    </ul>
                </div>

            </li>
        </ul>
    </div>
</section>

<script>
    var mainComparisonChart = function() {

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'mainComparisonChart',
                type: 'pie',
                backgroundColor: null,
                events:{
                    load:function(){
                        responsiveText(this,'#056bf3', true);
                    }
                }
            },
            title: {
                text: null
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: '70%',
                    startAngle: -90,
                    size: '210px',
                    borderWidth: 0,
                    dataLabels: {
                        connectorWidth: 3,
                        enabled: true,
                        useHTML: true,
                        distance:60,
                        softConnector: false,
                        connectorPadding:0,
                        format: '<div class="graph-label" style="border: 3px solid {point.color};"><p>{point.name}</p><p>{point.y}%</p></div>',
                        style: {
                            textShadow: false,
                        }
                    }
                }
            },
            series: [{
                data: [
                    {
                        name: 'Masculino',
                        y: <?php echo $destaques_campanha['sexo_m'];?>,
                        color: '#efefef'
                    },
                    {
                        name: 'Feminino',
                        y: <?php echo $destaques_campanha['sexo_f'];?>,
                        color: '#d4d5d6'
                    }
                ],
                name: ' '
            }]
        });
    };
</script>

<!-- MAIN ENGAGEMENT -->
<!-- ############################################################################################################### -->
<?php
$destaque_i = 0;
$posts_order = array_sort($posts, 'engajamento', SORT_DESC);
$sum = array_reduce($posts_order, function ($a, $b) {
    isset($a[$b['influenciador']]) ? $a[$b['influenciador']]['engajamento'] += $b['engajamento'] : $a[$b['influenciador']] = $b;
    return $a;
});
$posts_order = array_values($sum);
$posts_order = array_sort($posts_order, 'engajamento', SORT_DESC);
?>
<section class="slide slide__background--darkblue" id="engajamento">

    <!-- headline -->
    <div class="slide-headline slide__background--softblue">
        <div class="container container--center">
            <h3>ENGAJAMENTO</h3>
            <p>Participação dos principais posters da campanha</p>
        </div>
    </div>

    <div class="container container--ishead">
        <ul class="engagement">
            <li class="engagement__item">

                <!-- Graph -->
                <div class="graph">
                    <div id="mainEngagementChart" class="graph__item"></div>
                </div>

            </li>
            <li class="engagement__item">

                <!-- Top List by users -->
                <ul class="toplist-user">
                    <?php
                    foreach ($posts_order as $post) {
                        if(isset($post['influenciador'])){
                            if($destaque_i >= 4){
                                break;
                            }
                            $porcentagem = ($post['engajamento'] * 100) / $post['followers'];
                    ?>
                    <li class="user">
                        <figure class="user__figure">
                            <img src="<?php echo $post['usuario_foto'];?>" alt="" width="80" height="80" />
                            <span class="user__circle"></span>
                        </figure>
                        <div class="user__details">
                            <h3 class="user__name"><?php echo $post['influenciador'];?></h3>
                            <p class="user__text"><?php echo numero($post['engajamento']);?> de engajamento</p>
                        </div>
                    </li>
                    <?php
                            $destaque_i++;
                        }
                    }?>
                </ul>

            </li>
        </ul>
    </div>

</section>

<script>
    //Main Engagement Chart
    //----------------------------------------------------------------------
    var mainEngagementChart = function() {

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'mainEngagementChart',
                type: 'pie',
                backgroundColor: null,
                events:{
                    load:function(){
                        responsiveText(this,'#292e38', true);
                        rotateAngle(this);
                    }
                }
            },
            title: {
                text: null
            },
            tooltip: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    innerSize: '70%',
                    startAngle: 45,
                    size: '210px',
                    borderWidth: 0,
                    dataLabels: {
                        connectorWidth: 3,
                        enabled: true,
                        useHTML: true,
                        distance:50,
                        softConnector: false,
                        connectorPadding:0,
                        format: '<div class="graph-label" style="border: 3px solid {point.color};"><p>{point.name}</p><p>{point.y}%</p></div>',
                        style: {
                            textShadow: false,
                        }
                    }
                }
            },
            series: [{
                data: [
                    <?php
                    $destaque_i = 0;
                    $posts_order = array_sort($posts, 'engajamento', SORT_DESC);
                    $sum = array_reduce($posts_order, function ($a, $b) {
                        isset($a[$b['influenciador']]) ? $a[$b['influenciador']]['engajamento'] += $b['engajamento'] : $a[$b['influenciador']] = $b;
                        return $a;
                    });
                    $posts_order = array_values($sum);
                    $posts_order = array_sort($posts_order, 'engajamento', SORT_DESC);
                    foreach ($posts_order as $post) {
                        if(isset($post['influenciador'])){
                            if($destaque_i >= 1){
                                $influenciador_r[1] = 'Outros';
                                $engajamento_r[1] += round(($post['engajamento'] * 100) / ($engajamento['instagram']['geral'] + $engajamento['twitter']['geral'] + $engajamento['facebook']['geral'] + $engajamento['youtube']['geral']  + $engajamento['snapchat']['geral']));
                                $destaque_i++;
                            }
                            $influenciador_r[$destaque_i] = $post['influenciador'];
                            $engajamento_r[$destaque_i] = round(($post['engajamento'] * 100) / ($engajamento['instagram']['geral'] + $engajamento['twitter']['geral'] + $engajamento['facebook']['geral'] + $engajamento['youtube']['geral'] + $engajamento['snapchat']['geral']));
                            $destaque_i++;
                        }
                    }
                    if(isset($influenciador_r[0]) && !isset($influenciador_r[1])){
                    ?>
                    {
                        name: '<?php echo $influenciador_r[0];?>',
                        y: <?php echo $engajamento_r[0];?>,
                        color: '#d4d5d6'
                    }
                    <?php
                    }else{
                    ?>

                    {
                        name: '<?php echo $influenciador_r[0];?>',
                        y: <?php echo $engajamento_r[0];?>,
                        color: '#d4d5d6'
                    },
                    {
                        name: '<?php echo $influenciador_r[1];?>',
                        y: <?php echo $engajamento_r[1];?>,
                        color: '#b8b7b9'
                    },
                    <?php
                        // {
                        //   name: '<php //echo $influenciador_r[2];>',
                        //   y: <php echo $engajamento_r[2];>,
                        //   color: '#9B999C'
                        // },
                    ?>
                    <?php if(isset($influenciador_r[3])){
                    ?>
                    {
                        name: '<?php echo $influenciador_r[3];?>',
                        y: <?php echo $engajamento_r[3];?>,
                        color: '#efefef'
                    }
                    <?php
                    }
                    }
                    ?>
                ],
                name: ' '
            }]
        });
    };
</script>
<script>
    var dataFilter = <?php echo json_encode($posts) ?>;

    // Ações de Click //
    // ============================================================ //
    function clickFilter(area,field,button,className){
        $(area).on('click', button, function() {
            $(area).toggleClass(className);
            if( $(area).hasClass(className) ){
                $(area).removeClass(className+'--up');
                $(area).addClass(className+'--down');
                getDataTable(field,'sort');
            }else {
                $(area).removeClass(className+'--down');
                $(area).addClass(className+'--up');
                getDataTable(field,'reverse');
            }
        });
    };

    // Formatar data //
    // ============================================================ //
    function formattedDate(date) {
        return moment(date).format("DD/MM/YYYY");
    };

    // Função para ordernar //
    // ============================================================ //
    function sortable(data,order){
        return function(a, b) {
            if( typeof a[data] != "undefined" ){
                if (a[data] === b[data]) {
                    return 0;
                }
                if ( order === "sort" ){
                    return (a[data] < b[data]) ? -1 : 1;
                }
                if ( order === "reverse" ){
                    return (a[data] > b[data]) ? -1 : 1;
                }
            }
        };
    };

    // Função para criar tabela //
    // ============================================================ //
    function getDataTable(field,orderBy){

        // variaveis
        var divisor,
            porcentagem,
            array = [],
            count_Posts = 0,
            currentPost = 0;

        $('.tabela').empty();

        if(typeof dataFilter != "undefined"){

            // transfira todos os dados para um array //
            array = $.map(dataFilter, function(el) { return el });

            // Adiciona campo porcentagem aos dados //
            $.each( array, function( key, val ) {
                if( typeof val.influenciador != "undefined" ){
                    porcentagem = (array[key]['engajamento'] / array[key]['followers']) * 100;
                    array[key]['porcentagem'] = porcentagem;
                    count_Posts += 1;
                }
            });

            // filtra conforme campo e ordem //
            array.sort(sortable(field,orderBy));

            // carrega os dados em tabelas //
            divisor = Math.ceil(count_Posts / 12 - 1);
            for(var i = 0, divide = divisor; i < divide; i++){
                $('.tabela').append('<section class="slide slide__background--darkblue" id="tabela-users"><div class="container container--full"><div class="datatable"><table class="datatable__main"><thead class="datatable__head"><tr><th class="datatable__head-item datatable__left table__name">Nome<span class="table__name--down"></span><span class="table__name--up"></span></th><th class="datatable__head-item table__redesocial"></th><th class="datatable__head-item datatable__center">#</th><th class="datatable__head-item table__data">Data<span class="table__data--down"></span><span class="table__data--up"></span></th><th class="datatable__head-item table__engajamento">Engajamento<i class="tooltips tooltips--dark" alt="Soma das interações totais dividido pelo número de seguidores."></i><span class="table__engajamento--down"></span><span class="table__engajamento--up"></span></th><th class="datatable__head-item table__total">Int. Totais<span class="table__total--down"></span><span class="table__total--up"></span></th></tr></thead><tbody class="datatable__body" id="dataTable-'+i+'">');
                for(var contadorX = currentPost; contadorX <= currentPost+12; contadorX++){
                    if( typeof array[contadorX] != "undefined" ){
                        $('#dataTable-'+i).append('<tr><td class="datatable__body-item datatable__left">'+array[contadorX]['influenciador']+'</td><td class="datatable__body-item datatable__center"><img src="http://relatorios.plaaymedia.com/pages/relatorio/images/icon_'+array[contadorX]['redesocial'].replace(/[^A-Za-z]/g, "")+'_square.png" alt="'+array[contadorX]['redesocial'].replace(/[^A-Za-z]/g, "")+'" width="22" /></td><td class="datatable__body-item datatable__center">01</td><td class="datatable__body-item">'+formattedDate(array[contadorX]['data'])+'</td><td class="datatable__body-item">'+ array[contadorX]['porcentagem'].toFixed(2) +'%</td><td class="datatable__body-item">'+array[contadorX]['engajamento'].toLocaleString()+'</td></tr>');
                    }
                }
                currentPost += 13;
                $('.tabela').append('</tbody></table></div></div></section>');
            }
        }else{
            console.log( "Request Failed");
        };
    };

    $(document).ready(function(){

        $(".tabela").addClass('sort__name--up sort__redesocial--up sort__data--up sort__engajamento--up sort__total--up');

        clickFilter('.tabela','influenciador','.table__name','sort__name');
        clickFilter('.tabela','redesocial','.table__redesocial','sort__redesocial');
        clickFilter('.tabela','data','.table__data','sort__data');
        clickFilter('.tabela','porcentagem','.table__engajamento','sort__engajamento');
        clickFilter('.tabela','engajamento','.table__total','sort__total');

        getDataTable('influenciador','sort');
    });
</script>
<div class="tabela"></div>

<!-- MAIN COMMENTS -->
<!-- ############################################################################################################### -->
<section class="slide slide__background--softblue" id="comentarios">

    <!-- headline -->
    <div class="slide-headline">
        <div class="container container--center">
            <h3>COMENTÁRIOS</h3>
            <p>Destaques de comentários positivos, neutros e negativos</p>
        </div>
    </div>

    <div class="container container--ishead">

        <!-- Table's Comment -->
        <ul class="comments">
            <li class="comments__item">
                <h3 class="comments__title">Positivos (<?php echo round($comentarios_info['positivos'],1);?>%)</h3>
                <?php
                $positivo_max = 0;
                foreach ($comentario_destaque['positivo'] as $positivo) {
                ?>
                <figure class="card">
                    <img class="card__image" src="<?php echo $positivo['usuario_foto'];?>" alt="User Name" width="58" height="58"/>
                    <figcaption class="card__caption">
                        <h3 class="card__name"><?php echo $positivo['usuario'];?></h3>
                        <p class="card__text"><?php echo limitarTexto($positivo['comentario'], 90);?></p>
                    </figcaption>
                </figure>
                <?
                    $positivo_max++;
                    if($positivo_max >= 3){
                        break;
                    }
                }
                ?>
                <img class="comments__icons" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_happy.png" alt="Positivos" width="45" height="45"/>
            </li>
            <li class="comments__item">
                <h3 class="comments__title">Neutros (<?php echo round($comentarios_info['neutros'],1);?>%)</h3>
                <?php
                $positivo_max = 0;
                foreach ($comentario_destaque['neutro'] as $positivo) {
                ?>
                <figure class="card">
                    <img class="card__image" src="<?php echo $positivo['usuario_foto'];?>" alt="User Name" width="58" height="58"/>
                    <figcaption class="card__caption">
                        <h3 class="card__name"><?php echo $positivo['usuario'];?></h3>
                        <p class="card__text"><?php echo limitarTexto($positivo['comentario'], 90);?></p>
                    </figcaption>
                </figure>
                <?
                    $positivo_max++;
                    if($positivo_max >= 3){
                        break;
                    }
                }
                ?>
                <img class="comments__icons" src="<?php echo $page['relatorio'][1];?>/images/icon_smile.png" alt="Neutros" width="45" height="45"/>
            </li>
            <li class="comments__item">
                <h3 class="comments__title">Negativos (<?php echo round($comentarios_info['negativos'],1);?>%)</h3>
                <?php
                $positivo_max = 0;
                foreach ($comentario_destaque['negativo'] as $positivo) {
                ?>
                <figure class="card">
                    <img class="card__image" src="<?php echo $positivo['usuario_foto'];?>" alt="User Name" width="58" height="58"/>
                    <figcaption class="card__caption">
                        <h3 class="card__name"><?php echo $positivo['usuario'];?></h3>
                        <p class="card__text"><?php echo limitarTexto($positivo['comentario'],140);?></p>
                    </figcaption>
                </figure>
                <?
                    $positivo_max++;
                    if($positivo_max >= 3){
                        break;
                    }
                }
                ?>
                <img class="comments__icons" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_sad.png" alt="Negativos" width="45" height="45"/>
            </li>
        </ul>

    </div>
</section>

<!-- MAIN LOG -->
<!-- ############################################################################################################### -->
<section class="slide slide__background--softblue" id="como-nos-saimos">

    <!-- headline -->
    <div class="slide-headline">
        <div class="container container--center">
            <h3>COMO NOS SAÍMOS</h3>
            <p>Resumo geral do resultado da campanha</p>
        </div>
    </div>

    <script>
        gridster_json = <?php echo $destaques_campanha['como_nos_saimos'];?>
    </script>

    <!-- Gridster -->
    <div class="container container--ishead">
        <div class="gridster">
            <ul class="dragdrop"></ul>
        </div>
    </div>

</section>
