<!-- BLOG -->
<!-- ############################################################################################################### -->
<article class="theme--blog" id="Blog">

    <!-- Social blog -->
    <section class="slide">

        <div class="social-headline">
            <div class="social-headline__background">
                <img class="social-headline__logo" src="<?php echo $page['relatorio'][1];?>/images/icon_blog.png" alt="Blog" width="95" height="95" />
            </div>
            <div class="social-headline__counter">
                <div class="social-headline__counter-title">ENGAJAMENTO</div>
                <span class="social-headline__counter-number">8900</span>
            </div>
        </div>
        <div class="container container--ishead-social">
            <div class="container--fullflex">

                <div class="graph">
                    <div id="blogOneChart" class="graph__item"></div>
                    <div id="blogOneChartText" class="graph__text"></div>
                </div>

                <div class="slide__caption">
                    <p>Número de Posts</p>
                    <strong>2500</strong>
                </div>

            </div>
        </div>
    </section>
    <script>
        var blogOneChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'blogOneChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            $('#blogOneChartTotal').text(arraySum(this.series[0].processedYData));
                            responsiveText(this,'#FFFFFF', true);
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:50,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#C36706;"><p>{point.name}</p><p>{point.y}</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Page Views',
                            y: 60,
                            color: '#f57d00'
                        },
                        {
                            name: 'Views',
                            y: 60,
                            color: '#C36706'
                        }
                    ],
                    name: ' '
                }]
            },

                                             function(chart) {
                var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
                var textY = chart.plotTop  + (chart.plotHeight * 0.5);

                var innerText = '<div class="graph-label__inner graph-label__inner-social graph-label__inner-blog" >';
                innerText += '<p>Total</p>';
                innerText += '<p id="blogOneChartTotal"></p>';
                innerText += '</div>';

                $("#blogOneChartText").append(innerText);
                innerText = $('.graph-label__inner');
            });
        };
    </script>

    <!-- Datatable Blog -->
    <section class="slide">
        <div class="container container--full">

            <div class="datatable">
                <table class="datatable__main">
                    <thead class="datatable__head">
                        <tr>
                            <th class="datatable__head-item datatable__left">Nome</th>
                            <th class="datatable__head-item datatable__center">#</th>
                            <th class="datatable__head-item">Data</th>
                            <th class="datatable__head-item">Views</th>
                            <th class="datatable__head-item">Pageviews</th>
                            <th class="datatable__head-item">Int. Totais</th>
                        </tr>
                    </thead>
                    <tbody class="datatable__body">
                        <tr>
                            <td class="datatable__body-item datatable__left">teste</td>
                            <td class="datatable__body-item datatable__center">01</td>
                            <td class="datatable__body-item">000</td>
                            <td class="datatable__body-item">000</td>
                            <td class="datatable__body-item">000</td>
                            <td class="datatable__body-item">000</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </section>

    <!-- Social blog details -->
    <section class="slide slide--social">
        <div class="social-headline__details">
            <div class="social-headline__details-step">1</div>
        </div>
        <div class="container container--ishead-details">
            <div class="container--fullflex">
                <div class="user">
                    <figure class="user__figure">
                        <img class="circle__image" src="https://igcdn-photos-g-a.akamaihd.net/hphotos-ak-xtp1/t51.2885-19/11428190_797502083682454_1846765632_a.jpg" alt="" width="80" height="80" />
                        <span class="user__circle circle__image"></span>
                    </figure>
                    <div class="user__details">
                        <h3 class="user__name">78 - <strong>78 seguidores</strong></h3>
                        <p class="user__text">89% de engajamento <strong><i class="tooltips tooltips--dark" alt="Soma das interações totais dividido pelo número de seguidores."></i></strong></p>
                    </div>
                </div>
                <ul class="social-list">
                    <li class="social-list__item social-list__item--right">
                        <div class="social-post">
                            <div class="social-post__item">
                                <div class="social-post__item-data">
                                    <iframe class="social-post__item-media" src="" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <ul class="social-post__option">
                                <li class="social-post__option-item">
                                    <h3>Views</h3>
                                    <p>89</p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Page Views</h3>
                                    <p>90</p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="social-list__item social-list__item--left">
                        <div class="graph-bar">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Positivos</li>
                                <li class="graph-bar__label-item">Neutros</li>
                                <li class="graph-bar__label-item">Negativos</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:67%"></div>
                                    <p class="graph-bar__data-number">67%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:100%"></div>
                                    <p class="graph-bar__data-number">100%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:89%"></div>
                                    <p class="graph-bar__data-number">78%</p>
                                </li>
                            </ul>
                        </div>
                        <figure class="card social-card">
                            <img class="card__image social-card__image circle__image" src="https://igcdn-photos-g-a.akamaihd.net/hphotos-ak-xtp1/t51.2885-19/11428190_797502083682454_1846765632_a.jpg" alt="" width="58" height="58" />
                            <figcaption class="card__caption">
                                <h3 class="card__name">Destaque</h3>
                                <p class="card__text">teste</p>
                                <h4 class="card__user">tese</h4>
                            </figcaption>
                            <img class="card__icon" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_happy.png" alt="Positivo" width="40" height="40"/>
                        </figure>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Social blog details -->
    <section class="slide slide--social">
        <div class="container container--full">
            <div class="container--fullflex">
                <div class="graph-line">
                    <h3 class="graph-line__title">Crescimento de <div class="graph-line__legend graph__legend2"></div></h3>
                    <div class="graph-line__content">
                        <canvas class="graph-line__chart" id="blogThreeChart2"></canvas>
                    </div>
                </div>
                <ul class="container__three">
                    <li class="container__three-item">
                        <div class="time-clock">
                            <i class="fa fa-clock-o fa-5x"></i>
                            <h3 class="time-clock__name">Tempo em página</h3>
                            <strong class="time-clock__number">120min</strong>
                        </div>
                    </li>
                    <li class="container__three-item">
                        <div class="graph-bar">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Masculino</li>
                                <li class="graph-bar__label-item">Feminino</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:70%;"></div>
                                    <p class="graph-bar__data-number">70%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:30%;"></div>
                                    <p class="graph-bar__data-number">30%</p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="container__three-item">
                        <div class="toplist toplist-3">
                            <div class="toplist__left">
                                <div class="toplist__title">Top 3</div>
                            </div>
                            <ul class="toplist__right">
                                <li class="toplist__item">
                                    <div class="toplist__position circle__image">1</div>
                                    <div class="toplist__details">
                                        <p class="toplist__name">SÃ£o Paulo</p>
                                        <span class="toplist__text">50% de engajamento</span>
                                    </div>
                                </li>
                                <li class="toplist__item">
                                    <div class="toplist__position circle__image">2</div>
                                    <div class="toplist__details">
                                        <p class="toplist__name">Rio de Janeiro</p>
                                        <span class="toplist__text">30% de engajamento</span>
                                    </div>
                                </li>
                                <li class="toplist__item">
                                    <div class="toplist__position circle__image">3</div>
                                    <div class="toplist__details">
                                        <p class="toplist__name">ParanÃ¡</p>
                                        <span class="toplist__text">10% de engajamento</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
    </section>
    <script>
        var blogThreeChart2 = function() {
            var options = {
                animation: true,
                animationEasing: "easeInOutQuart",
                bezierCurve : false,
                scaleGridLineColor : "rgba(255,255,255,0.3)",
                scaleLineColor: "rgba(255,255,255,0.3)",
                scaleFontColor: "#fff",
                scaleFontFamily: "'Open Sans', Arial, sans-serif",
            };
            var data = {
                labels : ["20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015","20/84/2015"],
                datasets: [
                    {
                        label: "Views",
                        fillColor: "rgba(255,255,255,0.75)",
                        strokeColor: "transparent",
                        pointColor: "transparent",
                        pointStrokeColor: "#ffff",
                        pointHighlightFill: "#ffff",
                        pointHighlightStroke: "transparent",
                        data: [0,6,3,9,5,1,0,6,3,9,5,1]
                    },
                    {
                        label: "PageViews",
                        fillColor: "rgba(0,0,0,0.75)",
                        strokeColor: "transparent",
                        pointColor: "transparent",
                        pointStrokeColor: "#ffff",
                        pointHighlightFill: "#ffff",
                        pointHighlightStroke: "transparent",
                        data: [0,3,6,18,10,2,0,3,2,3,6,6]
                    }
                ]
            };
            var ctx = document.getElementById("blogThreeChart2").getContext("2d");
            var myLineChart = new Chart(ctx).Line(data,options);
            $(".graph__legend2").html(myLineChart.generateLegend());
        };
        blogThreeChart2();
    </script>

</article>
