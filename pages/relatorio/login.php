<?php
include_once "../../functions.php";
include_once "../../exibir_conteudo.php";
include_once "../../login-campanhas.php";
$dados_campanha = get_campanhas_login($_GET['id']);
?>
<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">
  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <!-- build:css(.) styles/vendor.css -->
  <!-- bower:css -->
  <link href="http://relatorios.plaaymedia.com/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <!-- endbower -->
  <!-- endbuild -->
  <!-- build:css(.tmp) styles/main.css -->
  <style>
  .wrapper{

    background: rgba(255,93,56,1);
    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgb100%,93,56,1)), color-stop(100%, rgba(255,41,101,1)));
    <?php
    if($dados_campanha['empresa'] == 2){?>
      background: -moz-linear-gradient(-45deg,rgb(128, 56, 133) 0%, rgb(32, 126, 186) 100%);
      background: -webkit-gradient(left top, left bottom, color-stop(0%, rgb100%,93,56,1)), color-stop(100%, rgba(255,41,101,1)));
      background: -webkit-linear-gradient(-45deg,rgb(128, 56, 133) 0%, rgb(32, 126, 186) 100%);
      background: -o-linear-gradient(-45deg,rgb(128, 56, 133) 0%, rgb(32, 126, 186) 100%);
      background: -ms-linear-gradient(-45deg,rgb(128, 56, 133) 0%, rgb(32, 126, 186) 100%);
      background: linear-gradient(-45deg,rgb(128, 56, 133) 0%, rgb(32, 126, 186) 100%);
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5d38', endColorstr='#ff2965', GradientType=0 );
      <?php
    }
    if($dados_campanha['empresa'] == 3){?>
      background: -moz-linear-gradient(-45deg,rgb(252, 156, 9) 0%, rgb(162, 109, 25) 100%);
      background: -webkit-linear-gradient(-45deg,rgb(252, 156, 9) 0%, rgb(162, 109, 25) 100%);
      background: -o-linear-gradient(-45deg,rgb(252, 156, 9) 0%, rgb(162, 109, 25) 100%);
      background: -ms-linear-gradient(-45deg,rgb(252, 156, 9) 0%, rgb(162, 109, 25) 100%);
      background: linear-gradient(-45deg,rgb(252, 156, 9) 0%, rgb(162, 109, 25) 100%)
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ff5d38', endColorstr='#ff2965', GradientType=0 );
      <?php
    }
    ?>
    height: 100%;
    width: 100%;
    display: block;
    position: absolute;
    top: 0;
    left: 0;
  }


  .login {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    max-width: 400px;
    /*height: 332px;*/
    overflow: hidden;
    display: inline-table;
    background: #ffffff;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
    -moz-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
    -ms-box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
    box-shadow: 2px 2px 5px rgba(0, 0, 0,0.3);
    z-index: 5;
  }

  .login .content {
    padding: 40px 60px;
  }

  .login .content .logo-plaaymedia {
    text-align: center;
    width: 100%;
    padding-bottom: 32px;
  }

  .login .content .login-form {
    padding: 0 10px;
  }

  .login .content .login-form fieldset {
    border: 2px solid #f2f2f2;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
  }

  .login .content .login-form label {
    width: 100%;
    margin-bottom: 0;
  }

  .login .content .login-form input{
    padding: 15px;
    width: 100%;
    border: none;
    border-top: 2px solid #f2f2f2;
    margin-top: -2px;
    font-size: 16px;
    outline: none;
    font-weight: normal;
    height: 55px;
    background: #fff;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;
  }
  .login .content .login-form select {
    padding: 15px;
    width: 100%;
    border: none;
    border-top: 2px solid #f2f2f2;
    margin-top: -2px;
    font-size: 16px;
    outline: none;
    font-weight: normal;
    height: 55px;
    background: #fff;
    -webkit-appearance:none;
    -moz-appearance:none;
    appearance:none;

    color:#A8A9A9;
  }
  .no-br{
    border-radius: 0;
  }
  .login .content .btn {
    width: 100%;
    padding: 10px;
    margin-top: 10px;
    font-size: 16px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    border-radius: 4px;
  }

  .alert {
    margin-bottom: -15px !important;
  }
  <?php
  if($dados_campanha['empresa'] == 3){
  ?>
    .btn-primary {
      color: #fff;
      background-color: #FD9809;
      border-color: #E29C23;
    }
    .btn-primary:hover {
      color: #fff;
      background-color: #A27019;
      border-color: #A27019;
    }
  <?php
  }
  ?>
  </style>
  <link href="http://relatorios.plaaymedia.com/css/animate.css" rel="stylesheet" type="text/css">
  <!-- endbuild -->
  <!-- build:js scripts/vendor/modernizr.js -->

</head>
<body>
  <!--[if lt IE 10]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
  <div class="wrapper">
    <div class="login animated <?php if (!isset($controller['message'])) { echo("bounceInDown"); } ?>">
      <?php require_once("../template/erros.php"); ?>
      <div class="content">
        <figure class="logo-plaaymedia" >
          <img src="<?php echo $page['home'][0];?>/img/logo-em-<?php echo $dados_campanha['empresa'];?>.png" width="250" />
        </figure>
        <form class="login-form" action="#" method="post">
          <input name="campanha-cliente" type="text" required value="<?php echo slug($dados_campanha['cliente']);?>" style="display:none"/>
          <input name="campanha-id" type="text" required value="<?php echo $dados_campanha['id'];?>" style="display:none"/>
          <fieldset>
            <label>
              <input name="campanha" type="text" required value="<?php echo $dados_campanha['nome'];?>" readonly/>
            </label>
            <label>
              <input name="senha" type="password" required placeholder="Senha" />
            </label>
          </fieldset>
          <button type="submit" class="btn btn-primary">Entrar</button>
        </form>
      </div>
    </div>

    <div class="push"></div>
  </div>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37490571-9', 'auto');
  ga('send', 'pageview');

  </script>
</body>
</html>
