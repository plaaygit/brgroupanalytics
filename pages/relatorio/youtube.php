<!-- YOUTUBE -->
<!-- ############################################################################################################### -->
<article class="theme--youtube" id="Youtube">

    <!-- Social youtube -->
    <section class="slide">

        <div class="social-headline">
            <div class="social-headline__background">
                <img class="social-headline__logo" src="<?php echo $page['relatorio'][1];?>/images/icon_youtube.png" alt="Youtube" width="95" height="95" />
            </div>
            <div class="social-headline__counter">
                <div class="social-headline__counter-title">ENGAJAMENTO</div>
                <span class="social-headline__counter-number"><?php echo numero($engajamento['youtube']['geral']);?></span>
            </div>
        </div>
        <div class="container container--ishead-social">
            <div class="container--fullflex">

                <div class="graph">
                    <div id="youtubeOneChart" class="graph__item"></div>
                    <div id="youtubeOneChartText" class="graph__text"></div>
                </div>

                <div class="slide__caption">
                    <p>Número de Posts</p>
                    <strong><?php echo numero($engajamento['youtube']['posts']);?></strong>
                </div>

            </div>
        </div>
    </section>
    <script>
        //Youtube Charts
        //----------------------------------------------------------------------
        var youtubeOneChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'youtubeOneChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            $('#youtubeOneChartTotal').text(arraySum(this.series[0].processedYData));
                            responsiveText(this,'#FFFFFF', true);
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:50,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#cd201f;"><p>{point.name}</p><p>{point.y}</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Comentarios',
                            y: <?php echo $engajamento['youtube']['comments'];?>,
                            color: '#b3221e'
                        },
                        {
                            name: 'Likes',
                            y:  <?php echo $engajamento['youtube']['likes'];?>,
                            color: '#cd201f'
                        }
                    ],
                    name: ' '
                }]
            },

                                             function(chart) {
                var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
                var textY = chart.plotTop  + (chart.plotHeight * 0.5);

                var innerText = '<div class="graph-label__inner graph-label__inner-social graph-label__inner-youtube" >';
                innerText += '<p>Total</p>';
                innerText += '<p id="youtubeOneChartTotal"></p>';
                innerText += '</div>';

                $("#youtubeOneChartText").append(innerText);
                innerText = $('.graph-label__inner');
            });
        };

        //----------------------------------------------------------------------
        var youtubeTwoChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'youtubeTwoChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            responsiveText(this,'#FFFFFF');
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:60,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#cd201f;"><p>{point.name}</p><p>{point.y}%</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Feminino',
                            y: <?php echo $destaques_campanha['sexo_f'];?>,
                            color: '#b3221e'
                        },
                        {
                            name: 'Masculino',
                            y: <?php echo $destaques_campanha['sexo_m'];?>,
                            color: '#cd201f'
                        }
                    ],
                    name: ' '
                }]
            });
        };
    </script>

    <!-- Datatable Youtube -->
    <?php
    $count_Posts = 0;
    foreach ($start as $post) {
        if(isset($post['youtube']['influenciador'])){
            $count_Posts++;
        }
    }
    foreach ($start as $key => $value) {
        if (key($value) == "youtube") {
            $posts_youtube[] = $value['youtube'];
        }
    }

    $currentPost = 0;
    $divisor = ceil($count_Posts / 12) - 1;
    for($ab = 0; $ab <= $divisor; $ab++){
    ?>
    <section class="slide">
        <div class="container container--full">

            <div class="datatable">
                <table class="datatable__main">
                    <thead class="datatable__head">
                        <tr>
                            <th class="datatable__head-item datatable__left">Nome</th>
                            <th class="datatable__head-item datatable__center">#</th>
                            <th class="datatable__head-item">Data</th>
                            <th class="datatable__head-item">Views</th>
                            <th class="datatable__head-item">Likes</th>
                            <th class="datatable__head-item">Dislikes</th>
                            <th class="datatable__head-item">Comentarios</th>
                            <th class="datatable__head-item">Int. Totais</th>
                        </tr>
                    </thead>
                    <tbody class="datatable__body">
                        <?php
        for($contadorX = $currentPost; $contadorX <= $currentPost+12; $contadorX++){
            $porcentagem = ($posts_youtube[$contadorX]['engajamento'] * 100) / $posts_youtube[$contadorX]['followers'];
            if(isset($posts_youtube[$contadorX]['influenciador'])){
                        ?>
                        <tr>
                            <td class="datatable__body-item datatable__left"><?php echo $posts_youtube[$contadorX]['influenciador'];?></td>
                            <td class="datatable__body-item datatable__center">01</td>
                            <td class="datatable__body-item"><?php echo $posts_youtube[$contadorX]['data'];?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_youtube[$contadorX]['yt_views']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_youtube[$contadorX]['yt_likes']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_youtube[$contadorX]['yt_dislikes']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_youtube[$contadorX]['yt_comments']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_youtube[$contadorX]['interacoes']);?></td>
                        </tr>
                        <?
            }
        }
        $currentPost += 13;
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
    <?php
    }
    ?>

    <!-- Social youtube details -->
    <?php
    $count_posts_twitter = 0;
    foreach ($start as $post) {
        if(isset($post['youtube'])){
            $post = $post['youtube'];
            if(isset($post['influenciador'])){
                $count_posts_youtube++;
                $porcentagem = ($post['interacoes'] * 100) / $post['yt_inscritos'];
                if($porcentagem <= 5){
                    $tipo_engajamento = 'Baixo';
                }
                if($porcentagem >= 7){
                    $tipo_engajamento = 'Regular';
                }
                if($porcentagem >= 10){
                    $tipo_engajamento = 'Médio';
                }
                if($porcentagem >= 15){
                    $tipo_engajamento = 'Alto';
                }
                if($porcentagem >= 30){
                    $tipo_engajamento = 'Muito alto';
                }
    ?>
    <section class="slide slide--social">
        <div class="social-headline__details">
            <div class="social-headline__details-step"><?php echo $count_posts_youtube;?></div>
        </div>
        <div class="container container--ishead-details">
            <div class="container--fullflex">
                <div class="user">
                    <figure class="user__figure">
                        <img class="circle__image" src="<?php echo $post['usuario_foto'];?>" alt="<?php echo $post['influenciador'];?>" width="80" height="80" />
                        <span class="user__circle circle__image"></span>
                    </figure>
                    <div class="user__details">
                        <h3 class="user__name"><?php echo $post['influenciador'];?> - <strong><?php echo numero($post['yt_inscritos']);?> seguidores</strong></h3>
                        <p class="user__text"><?php echo round($porcentagem,3);?>% de engajamento <strong><?php //echo $tipo_engajamento;?><i class="tooltips tooltips--dark" alt="Soma das interações totais dividido pelo número de seguidores."></i></strong></p>
                    </div>
                </div>
                <ul class="social-list">
                    <li class="social-list__item social-list__item--right">
                        <div class="social-post">
                            <div class="social-post__item">
                                <div class="social-post__item-data">
                                    <iframe class="social-post__item-video" width="525" height="295" src="https://www.youtube.com/embed/<?php echo $post['post_id'];?>?autoplay=0&showinfo=0&controls=0&hd=1" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <ul class="social-post__option">
                                <li class="social-post__option-item">
                                    <h3>Views</h3>
                                    <p><?php echo numero($post['yt_views']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Likes</h3>
                                    <p><?php echo numero($post['yt_likes']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Comments</h3>
                                    <p><?php echo numero($post['yt_comments']);?></p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="social-list__item social-list__item--left">
                        <div class="graph-bar">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Positivos</li>
                                <li class="graph-bar__label-item">Neutros</li>
                                <li class="graph-bar__label-item">Negativos</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['positivo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['positivo'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['neutro'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['neutro'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['negativo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['negativo'];?>%</p>
                                </li>
                            </ul>
                        </div>
                        <?php if(isset($post['comentarios']['comentarios_destaque'][0]['comentario'])){ ?>
                        <figure class="card social-card">
                            <img class="card__image social-card__image circle__image" src="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario_foto'];?>" alt="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?>" width="58" height="58" />
                            <figcaption class="card__caption">
                                <h3 class="card__name">Destaque</h3>
                                <p class="card__text"><?php echo $post['comentarios']['comentarios_destaque'][0]['comentario'];?></p>
                                <h4 class="card__user"><?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?></h4>
                            </figcaption>
                            <img class="card__icon" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_happy.png" alt="Positivo" width="40" height="40"/>
                        </figure>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <!-- Social youtube details -->
    <?php if (isset($post['youtube'])){ ?>
    <section class="slide slide--social">
        <div class="container container--full">
            <div class="container--fullflex">
                <div class="graph-line">
                    <h3 class="graph-line__title">Crescimento de Views</h3>
                    <div class="graph-line__content">
                        <canvas class="graph-line__chart" id="youtubeThreeChart<?php echo $count_posts_youtube;?>"></canvas>
                    </div>
                </div>
                <ul class="container__three">
                    <li class="container__three-item">
                        <div class="time-counter">
                            <div class="time-counter__initial">0:00</div>
                            <div class="time-counter__bar">
                                <div class="time-counter__bar-circle" style="left: <?php echo ($post['youtube']['assistido']['assistido'] * 100) / $post['youtube']['assistido']['total'];?>%;">
                                    <div class="time-counter__bar-caption">Tempo médio</div>
                                    <div class="time-counter__bar-current"><?php echo gmdate("i:s", $post['youtube']['assistido']['assistido']);?></div>
                                </div>
                            </div>
                            <div class="time-counter__final"><?php echo gmdate("i:s", $post['youtube']['assistido']['total']);?></div>
                        </div>
                    </li>
                    <li class="container__three-item">
                        <div class="graph-bar graph-bar-youtube">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Masculino</li>
                                <li class="graph-bar__label-item">Feminino</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['youtube']['sexo']['sexo_m'];?>%;"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['youtube']['sexo']['sexo_m'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['youtube']['sexo']['sexo_f'];?>%;"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['youtube']['sexo']['sexo_f'];?>%</p>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <script>
        var youtubeThreeChart<?php echo $count_posts_youtube;?> = function() {
            var options = {
                animation: true,
                animationEasing: "easeInOutQuart",
                bezierCurve : false,
                scaleGridLineColor : "rgba(255,255,255,0.3)",
                scaleLineColor: "rgba(255,255,255,0.3)",
                scaleFontColor: "#fff",
                scaleFontFamily: "'Open Sans', Arial, sans-serif",
            };
            var data = {
                labels : [
                    <?php
                                       $i_avie = 0;
                                       $len = count($post['youtube']['data']);
                                       foreach ($post['youtube']['data'] as $yt ) {
                                           if ($i_avie == $len - 1) {
                                               echo '"'.$yt['data'].'"';
                                           }else{
                                               echo '"'.$yt['data'].'",';
                                           }
                                           $i_avie++;
                                       }
                    ?>
                ],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(255,255,255,0.75)",
                        strokeColor: "transparent",
                        pointColor: "transparent",
                        pointStrokeColor: "#ffff",
                        pointHighlightFill: "#ffff",
                        pointHighlightStroke: "transparent",
                        data: [
                            <?php
                                       $i_aviee = 0;
                                       $len = count($post['youtube']['data']);
                                       foreach ($post['youtube']['data'] as $yt ) {
                                           if ($i_aviee == $len - 1) {
                                               echo '"'.$yt['views'].'"';
                                           }else{
                                               echo '"'.$yt['views'].'",';
                                           }
                                           $i_aviee++;
                                       }
                            ?>]
                    }
                ]
            };
            var ctx = document.getElementById("youtubeThreeChart<?php echo $count_posts_youtube;?>").getContext("2d");
            new Chart(ctx).Line(data,options);
        };
        youtubeThreeChart<?php echo $count_posts_youtube;?>();
    </script>
    <?
                                      }
            }
        }
    }
    ?>


</article>
