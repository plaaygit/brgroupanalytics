//responsive function
//----------------------------------------------------------------------
function responsiveText(data, color, condition){
    var datagraph = function(){
        data.series[0].update({
            dataLabels: {
                distance: -5,
                connectorWidth:1,
                connectorPadding:10,
                format:'<p class="graph-label__mobile" style="background: {point.color};">{point.name} <br/> {point.percentage}%</p>',
                color: color
            }
        }, true);
    }
    if ( condition ){
        if ($('.container').width() < 678) {
            datagraph();
        }
    }else {
        datagraph();
    }
}

//Sum function
//----------------------------------------------------------------------
function arraySum(data) {
    var sum = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i]) {
            sum += data[i];
        }
    }
    return sum;
}

// Angle rotate function
//----------------------------------------------------------------------
function rotateAngle(data){
  var angle;
  var tt = data.series[0].points.length;
  switch(tt){
    case ( 1 || 3 ):
    angle = 0;
    break;
    case 2:
    angle = 90;
    break;
    case 4:
    angle = 45;
    break;
  }
  data.series[0].update({
      startAngle: angle
  });
}

//link social
//----------------------------------------------------------------------
function socialClick(){
    $(document).on("click",".icon_graph",function(){
        var item = $(this).attr("alt");
        $('html, body').animate({scrollTop: $("#"+item).offset().top - 65}, 1000);
    });
};
