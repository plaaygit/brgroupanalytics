//Variaveis globais
var gridster;
var gridster_json;
var gridster_data;
var gridster_tipo;

//Ordenar boxes no grid
serializations = Gridster.sort_by_row_and_col_asc(gridster_json);

//Função para determinar tipos de conteudo
gridster_tipo = function(tipo,dado){
    switch(tipo) {
        case 1:
            return '<li class="dragdrop__item">'+
                '<div class="dragdrop__box">'+
                '<div class="dragdrop__item-icon">'+
                '<img class="circle__image" src="'+ dado.image +'" width="85" height="85" />'+
                '</div>'+
                '<p class="dragdrop__item-text">'+ dado.text +'</p>'+
                '</div>'+
                '</li>'
            break;
        case 2:
            return '<li class="dragdrop__item">'+
                '<div class="dragdrop__box-padding">'+
                '<h3 class="dragdrop__item-title">'+ dado.title +'</h3>'+
                '<p class="dragdrop__item-text">'+ dado.text +'</p>'+
                '</div>'+
                '</li>'
            break;
        case 3:
            return '<li class="dragdrop__item">'+
                '<img class="dragdrop__item-image" src="'+ dado.image +'" />'+
                '</li>'
            break;
        case 4:
            return '<li class="dragdrop__item">'+
                '<div class="dragdrop__box">'+
                '<div class="dragdrop__item-icon">'+
                '<i class="circle__image fa '+dado.icon+' fa-3"></i>'+
                '</div>'+
                '<p class="dragdrop__item-text">'+ dado.text +'</p>'+
                '</div>'+
                '</li>'
            break;
        default:
            console.log("Tipo indefinido");
    }
};

//Função para carregar itens
gridster_data = function(){
    gridster.remove_all_widgets();
    $.each(serializations, function() {
        if ('key' in this !== 'null' && 'key' in this !== 'undefined') {
            gridster.add_widget(gridster_tipo(this.id, this) , this.size_x, this.size_y, this.col, this.row);
        } else {
            console.log("Erro ao carregar dados");
        }
    });
};

// Cores slides
function getColor(item){
    var color;
    switch(item){
        case 'Facebook':
            return color = "#3b5998"
            break;
        case 'Twitter':
            return color = "#00aeeb"
            break;
        case 'Instagram':
            return color = "#3f729b"
            break;
        case 'Snapchat':
            return color = "#fffc50"
            break;
        case 'Youtube':
            return color = "#cd201f"
            break;
        default:
            return color = "#056bf3"
            break;
    }
    return color;
}

function socialColor(){
    var name,social;
    $('article').each(function(){
        name = '#'+$(this).attr('id');
        social = $(name).prev().attr('id');
        $(name+" .social-headline").css('background',getColor(social));
    });
}

function headlineColor(){
    $('.social-headline__details').each(function(){
        var t = $(this).closest('section').prev().css('background');
        $(this).css('background',t);
    });
}

$(document).ready(function() {

    //Instacianção do plugin Gridster.js
    gridster = $(".gridster ul").gridster({
        widget_base_dimensions: [280, 150],
        widget_margins: [16, 16]
    }).data('gridster').disable();

    //chamada da função para carregar itens
    gridster_data();

    //Tooltip
    var tooltip = $('.tooltips');
    tooltip.each(function(index){
        tooltip[index].innerHTML = tooltip[index].innerHTML +
            ' <sup class="tooltip">'+
            '<i class="tooltip__icon fa fa-info-circle">'+
            '<span class="tooltip__text">'+tooltip[index].getAttribute("alt")+'</span>'+
            '</i>'+
            '</sup>';
    });

    socialColor();
    headlineColor();

    if ($('.slide').width() >= 1280) {
        $.scrollify({section : ".slide",setHeights: false,});
    }

});

// twitter
twemoji.parse(document.body,  {size: 36});

// Loading
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
        return null;
    }
    else{
        return results[1] || 0;
    }
}
if ($.urlParam('bypass')){
    $('.loading-campaign').hide();
}else{
    $(window).bind("load", function() {
        setTimeout(function(){
            $('.loading-campaign').fadeOut();
        },2000);
    });
}
