<!-- INSTAGRAM -->
<!-- ############################################################################################################### -->
<article class="theme--instagram" id="Instagram">

    <!-- Social instagram -->
    <section class="slide">

        <div class="social-headline">
            <div class="social-headline__background">
                <img class="social-headline__logo" src="<?php echo $page['relatorio'][1];?>/images/icon_instagram.png" alt="Instagram" width="95" height="95" />
            </div>
            <div class="social-headline__counter">
                <div class="social-headline__counter-title">MARCAÇÕES</div>
                <span class="social-headline__counter-number"><?php echo $engajamento['instagram']['marcacoes'];?></span>
            </div>
        </div>

        <div class="container container--ishead-social">
            <div class="container--fullflex">

                <div class="graph">
                    <div id="instagramChart" class="graph__item"></div>
                    <div id="instagramChartText" class="graph__text"></div>
                </div>

                <div class="slide__caption">
                    <p>Número de Posts</p>
                    <strong><?php echo $engajamento['instagram']['posts'];?></strong>
                </div>

            </div>
        </div>
    </section>
    <script>
        //Instagram Chart
        //----------------------------------------------------------------------//
        var instagramChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'instagramChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            $('#instagramChartTotal').text(arraySum(this.series[0].processedYData));
                            responsiveText(this,'#FFFFFF', true);
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:50,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#3f729b;"><p>{point.name}</p><p>{point.y}</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Comentários',
                            y: <?php echo $engajamento['instagram']['comments'];?>,
                            color: '#38668a'
                        },
                        {
                            name: 'Likes',
                            y: <?php echo $engajamento['instagram']['likes'];?>   ,
                            color: '#3f729b'
                        }
                    ],
                    name: ' '
                }]
            },

                                             function(chart) {
                var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
                var textY = chart.plotTop  + (chart.plotHeight * 0.5);

                var innerText = '<div class="graph-label__inner graph-label__inner-social graph-label__inner-instagram" >';
                innerText += '<p>Total</p>';
                innerText += '<p id="instagramChartTotal"></p>';
                innerText += '</div>';

                $("#instagramChartText").append(innerText);
                innerText = $('.graph-label__inner');
            });
        };
    </script>

    <!-- Datatable Instagram -->
    <?php
    $count_Posts = 0;
    foreach ($start as $post) {
        if(isset($post['instagram']['influenciador'])){
            $count_Posts++;
        }
    }
    foreach ($start as $key => $value) {
        if (key($value) == "instagram") {
            $posts_instagram[] = $value['instagram'];
        }
    }

    $currentPost = 0;
    $divisor = ceil($count_Posts / 12) - 1;
    for($ab = 0; $ab <= $divisor; $ab++){
    ?>
    <section class="slide">
        <div class="container container--full">

            <div class="datatable">
                <table class="datatable__main">
                    <thead class="datatable__head">
                        <tr>
                            <th class="datatable__head-item datatable__left">Nome</th>
                            <th class="datatable__head-item datatable__center">#</th>
                            <th class="datatable__head-item">Data</th>
                            <th class="datatable__head-item">Likes</th>
                            <th class="datatable__head-item">Comentários</th>
                            <th class="datatable__head-item">Marcações</th>
                            <th class="datatable__head-item">Int. Totais</th>
                        </tr>
                    </thead>
                    <tbody class="datatable__body">
                        <?php
        for($contadorX = $currentPost; $contadorX <= $currentPost+12; $contadorX++){
            $porcentagem = ($posts_instagram[$contadorX]['engajamento'] * 100) / $posts_instagram[$contadorX]['followers'];
            if(isset($posts_instagram[$contadorX]['influenciador'])){
                        ?>
                        <tr>
                            <td class="datatable__body-item datatable__left"><?php echo $posts_instagram[$contadorX]['influenciador'];?></td>
                            <td class="datatable__body-item datatable__center">01</td>
                            <td class="datatable__body-item"><?php echo $posts_instagram[$contadorX]['data'];?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_instagram[$contadorX]['in_likes']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_instagram[$contadorX]['in_comments']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_instagram[$contadorX]['in_marcacoes']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_instagram[$contadorX]['interacoes']);?></td>
                        </tr>
                        <?
            }
        }
        $currentPost += 13;
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
    <?php
    }
    ?>

    <!-- Social instagram details -->
    <?php
    $count_posts_instagram = 0;
    foreach ($start as $post) {
        if(isset($post['instagram'])){
            $post = $post['instagram'];
            if(isset($post['influenciador'])){
                $count_posts_instagram++;
                $porcentagem = ($post['interacoes'] * 100) / $post['in_followers'];
                if($porcentagem >= 0.5){
                    $tipo_engajamento = 'Regular';
                }
                if($porcentagem >= 1){
                    $tipo_engajamento = 'Médio';
                }
                if($porcentagem >= 1.5){
                    $tipo_engajamento = 'Alto';
                }
                if($porcentagem >= 2.5){
                    $tipo_engajamento = 'Muito alto';
                }
    ?>
    <section class="slide slide--social">
        <div class="social-headline__details">
            <div class="social-headline__details-step"><?php echo $count_posts_instagram;?></div>
        </div>
        <div class="container container--ishead-details">
            <div class="container--fullflex">

                <div class="user">
                    <figure class="user__figure">
                        <img class="circle__image" src="<?php echo $post['usuario_foto'];?>" alt="<?php echo $post['influenciador'];?>" width="80" height="80" />
                        <span class="user__circle circle__image"></span>
                    </figure>
                    <div class="user__details">
                        <h3 class="user__name"><?php echo $post['influenciador'];?> - <strong><?php echo numero($post['in_followers']);?> Seguidores</strong></h3>
                        <p class="user__text"><?php echo round($porcentagem,2);?>% de engajamento <strong><?php //echo $tipo_engajamento;?><i class="tooltips tooltips--dark" alt="Soma das interações totais dividido pelo número de seguidores."></i></strong></p>
                    </div>
                </div>
                <ul class="social-list">
                    <li class="social-list__item social-list__item--right">
                        <div class="social-post">
                            <div class="social-post__item">
                                <div class="social-post__item-data">
                                    <a href="http://instagram.com/p/<?php echo $post['post_id'];?>" target="_blank"><img src="<?php echo $post['foto'];?>"  height="320"/></a>
                                    <div class="social-post__data-caption">
                                        <p><?php echo limitarTexto($post['legenda'], 140);?></p>
                                    </div>
                                </div>
                            </div>
                            <ul class="social-post__option">
                                <li class="social-post__option-item">
                                    <h3>Likes</h3>
                                    <p><?php echo numero($post['in_likes']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Comments</h3>
                                    <p><?php echo numero($post['in_comments']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Marcações</h3>
                                    <p><?php echo numero($post['in_marcacoes']);?></p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="social-list__item social-list__item--left">
                        <div class="graph-bar">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Positivos</li>
                                <li class="graph-bar__label-item">Neutros</li>
                                <li class="graph-bar__label-item">Negativos</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['positivo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['positivo'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['neutro'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['neutro'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['negativo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['negativo'];?>%</p>
                                </li>
                            </ul>
                        </div>
                        <?php if(isset($post['comentarios']['comentarios_destaque'][0]['comentario'])){ ?>
                        <figure class="card social-card">
                            <img class="card__image social-card__image circle__image" src="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario_foto'];?>" alt="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?>" width="58" height="58" />
                            <figcaption class="card__caption">
                                <h3 class="card__name">Destaque</h3>
                                <p class="card__text"><?php echo limitarTexto($post['comentarios']['comentarios_destaque'][0]['comentario'],200);?></p>
                                <h4 class="card__user">@<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?></h4>
                            </figcaption>
                            <img class="card__icon" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_happy.png" alt="Positivo" width="40" height="40"/>
                        </figure>
                        <?php }?>
                    </li>
                </ul>

            </div>
        </div>
    </section>
    <?php
            }
        }
    }
    ?>

</article>
