<!-- TWITTER -->
<!-- ############################################################################################################### -->
<article class="theme--twitter" id="Twitter">

    <!-- Social twitter -->
    <section class="slide">

        <!-- Social Headline -->
        <div class="social-headline">
            <div class="social-headline__background">
                <img class="social-headline__logo" src="<?php echo $page['relatorio'][1];?>/images/icon_twitter.png" alt="Twitter" width="95" height="95" />
            </div>
            <div class="social-headline__counter">
                <div class="social-headline__counter-title">ENGAJAMENTO</div>
                <span class="social-headline__counter-number"><?php echo numero($engajamento['twitter']['geral']);?></span>
            </div>
        </div>

        <div class="container container--ishead-social">
            <div class="container--fullflex">

                <!-- Social Graph -->
                <div class="graph">
                    <div id="twitterChart" class="graph__item"></div>
                    <div id="twitterChartText" class="graph__text"></div>
                </div>

                <!-- Social Caption -->
                <div class="slide__caption">
                    <p>Número de Posts</p>
                    <strong><?php echo numero($engajamento['twitter']['posts']);?></strong>
                </div>

            </div>
        </div>
    </section>
    <script>
        //Twitter Chart
        //----------------------------------------------------------------------
        var twitterChart = function() {

            var chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'twitterChart',
                    type: 'pie',
                    backgroundColor: null,
                    events:{
                        load:function(){
                            $('#twitterChartTotal').text(arraySum(this.series[0].processedYData));
                            responsiveText(this,'#FFFFFF', true);
                            rotateAngle(this);
                        }
                    }
                },
                title: {
                    text: null
                },
                tooltip: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        innerSize: '70%',
                        startAngle: 90,
                        size: '210px',
                        borderWidth: 0,
                        dataLabels: {
                            connectorWidth: 3,
                            enabled: true,
                            useHTML: true,
                            distance:50,
                            softConnector: false,
                            connectorPadding:0,
                            format: '<div class="graph-label" style="border: 3px solid {point.color};color:#00aeeb;"><p>{point.name}</p><p>{point.y}</p></div>',
                            style: {
                                textShadow: false,
                            }
                        }
                    }
                },
                series: [{
                    data: [
                        {
                            name: 'Replies',
                            y: <?php echo $engajamento['twitter']['replies'];?>,
                            color: '#1b9cd0'
                        },
                        {
                            name: 'Favs',
                            y: <?php echo $engajamento['twitter']['favs'];?>,
                            color: '#0690c1'
                        },
                        {
                            name: 'RTs',
                            y: <?php echo $engajamento['twitter']['rts'];?>   ,
                            color: '#00aeeb'
                        }
                    ],
                    name: ' '
                }]
            },

                                             function(chart) {
                var textX = chart.plotLeft + (chart.plotWidth  * 0.5);
                var textY = chart.plotTop  + (chart.plotHeight * 0.5);

                var innerText = '<div class="graph-label__inner graph-label__inner-social graph-label__inner-twitter" >';
                innerText += '<p>Total</p>';
                innerText += '<p id="twitterChartTotal"></p>';
                innerText += '</div>';

                $("#twitterChartText").append(innerText);
                innerText = $('.graph-label__inner');
            });
        };
    </script>

    <!-- Datatable twitter -->
    <?php
    $count_Posts = 0;
    foreach ($start as $post) {
        if(isset($post['twitter']['influenciador'])){
            $count_Posts++;
        }
    }
    foreach ($start as $key => $value) {
        if (key($value) == "twitter") {
            $posts_twitter[] = $value['twitter'];
        }
    }

    $currentPost = 0;
    $divisor = ceil($count_Posts / 12) - 1;
    for($ab = 0; $ab <= $divisor; $ab++){
    ?>
    <section class="slide">
        <div class="container container--full">

            <div class="datatable">
                <table class="datatable__main">
                    <thead class="datatable__head">
                        <tr>
                            <th class="datatable__head-item datatable__left">Nome</th>
                            <th class="datatable__head-item datatable__center">#</th>
                            <th class="datatable__head-item">Data</th>
                            <th class="datatable__head-item">RT's</th>
                            <th class="datatable__head-item">Favoritos</th>
                            <th class="datatable__head-item">Replies</th>
                            <th class="datatable__head-item">Int. Totais</th>
                        </tr>
                    </thead>
                    <tbody class="datatable__body">
                        <?php
        for($contadorX = $currentPost; $contadorX <= $currentPost+12; $contadorX++){
            $porcentagem = ($posts_twitter[$contadorX]['engajamento'] * 100) / $posts_twitter[$contadorX]['followers'];
            if(isset($posts_twitter[$contadorX]['influenciador'])){
                        ?>
                        <tr>
                            <td class="datatable__body-item datatable__left"><?php echo $posts_twitter[$contadorX]['influenciador'];?></td>
                            <td class="datatable__body-item datatable__center">01</td>
                            <td class="datatable__body-item"><?php echo $posts_twitter[$contadorX]['data'];?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_twitter[$contadorX]['tw_rts']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_twitter[$contadorX]['tw_favs']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_twitter[$contadorX]['tw_replies']);?></td>
                            <td class="datatable__body-item"><?php echo numero($posts_twitter[$contadorX]['interacoes']);?></td>
                        </tr>
                        <?
            }
        }
        $currentPost += 13; ?>
                    </tbody>
                </table>
            </div>

        </div>
    </section>
    <?php
    }
    ?>

    <!-- Social twitter details -->
    <?php
    $count_posts_twitter = 0;
    foreach ($start as $post) {
        if(isset($post['twitter'])){
            $post = $post['twitter'];
            if(isset($post['influenciador'])){
                $count_posts_twitter++;
                $porcentagem = ($post['interacoes'] * 100) / $post['tw_followers'];
                if($porcentagem <= 0.1){
                    $tipo_engajamento = 'Baixo';
                }
                if($porcentagem >= 0.2){
                    $tipo_engajamento = 'Regular';
                }
                if($porcentagem >= 0.5){
                    $tipo_engajamento = 'Médio';
                }
                if($porcentagem >= 0.75){
                    $tipo_engajamento = 'Alto';
                }
                if($porcentagem >= 1){
                    $tipo_engajamento = 'Muito alto';
                }
    ?>
    <section class="slide slide--social">

        <div class="social-headline__details">
            <div class="social-headline__details-step"><?php echo $count_posts_twitter;?></div>
        </div>

        <div class="container container--ishead-details">
            <div class="container--fullflex">

                <div class="user">
                    <figure class="user__figure">
                        <img class="circle__image" src="<?php echo $post['usuario_foto'];?>" alt="<?php echo $post['influenciador'];?>" width="80" height="80" />
                        <span class="user__circle circle__image"></span>
                    </figure>
                    <div class="user__details">
                        <h3 class="user__name"><?php echo $post['influenciador'];?> - <strong><?php echo numero($post['tw_followers']);?> seguidores</strong></h3>
                        <p class="user__text"><?php echo round($porcentagem,3);?>% de engajamento - <strong><?php //echo $tipo_engajamento;?><i class="tooltips tooltips--dark" alt="Soma das interações totais dividido pelo número de seguidores."></i></strong></p>
                    </div>
                </div>

                <ul class="social-list">
                    <li class="social-list__item social-list__item--right">
                        <div class="social-post">
                            <div class="social-post__item">
                                <a href="http://twitter.com/<?php echo $post['username']?>/status/<?php echo $post['post_id']?>" target="_blank">  <div class="social-post__item-data">
                                    <p class="social-post__data-twitter"><strong>@<?php echo $post['username'];?></strong> <?php echo $post['legenda'];?></p>
                                    </div>
                                </a>
                            </div>
                            <ul class="social-post__option">
                                <li class="social-post__option-item">
                                    <h3>RTs</h3>
                                    <p><?php echo numero($post['tw_rts']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>FAVs</h3>
                                    <p><?php echo numero($post['tw_favs']);?></p>
                                </li>
                                <li class="social-post__option-item">
                                    <h3>Replies</h3>
                                    <p><?php echo numero($post['tw_replies']);?></p>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="social-list__item social-list__item--left">
                        <div class="graph-bar">
                            <ul class="graph-bar__label">
                                <li class="graph-bar__label-item">Positivos</li>
                                <li class="graph-bar__label-item">Neutros</li>
                                <li class="graph-bar__label-item">Negativos</li>
                            </ul>
                            <ul class="graph-bar__data">
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['positivo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['positivo'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['neutro'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['neutro'];?>%</p>
                                </li>
                                <li class="graph-bar__data-item">
                                    <div class="graph-bar__data-bar" style="width:<?php echo $post['comentarios']['sentimento']['negativo'];?>%"></div>
                                    <p class="graph-bar__data-number"><?php echo $post['comentarios']['sentimento']['negativo'];?>%</p>
                                </li>
                            </ul>
                        </div>
                        <?php if(isset($post['comentarios']['comentarios_destaque'][0]['comentario'])){ ?>
                        <figure class="card social-card">
                            <img class="card__image social-card__image circle__image" src="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario_foto'];?>" alt="<?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?>" width="58" height="58" />
                            <figcaption class="card__caption">
                                <h3 class="card__name">Destaque</h3>
                                <p class="card__text">
                                    <?php
                                                                                                       $post['comentarios']['comentarios_destaque'][0]['comentario'] = preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<u><a href="$1" target="_blank" style="text-decoration:none; color:#292E38">$1</a></u>',$post['comentarios']['comentarios_destaque'][0]['comentario']);
                                                                                                       echo $post['comentarios']['comentarios_destaque'][0]['comentario'];?>
                                </p>
                                <h4 class="card__user"><?php echo $post['comentarios']['comentarios_destaque'][0]['usuario'];?></h4>
                            </figcaption>
                            <img class="card__icon" src="<?php echo $page['relatorio'][1];?>/images/icon_smile_happy.png" alt="Positivo" width="40" height="40"/>
                        </figure>
                        <?php } ?>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <?php
            }
        }
    }
    ?>

</article>
