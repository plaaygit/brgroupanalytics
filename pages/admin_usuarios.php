<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['admin']['usuarios']['status'] = true;
$dados = get_users();
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Usuários <span><a href="nova_campanha.php">criar novo</a></span></h1>
  <div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
      <thead>
        <tr>
          <th>Usuário</th>
          <th>Empresa</th>
          <th class="hidden-xs">Tipo</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dados as $usuario) {
          echo '<tr class="odd gradeX">
          <td><a href="'.$page['editar_post'][0].'/'.$usuario['id'].'">'.$usuario['usuario'].'</a></td>
          <td>'.$usuario['empresa-nome'].'</a></td>
          <td class="hidden-xs">'.$usuario['role'].'</td>
          </tr>';
        }

        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
$footer = <<<EOF
<script>
$(document).ready(function() {
  $('#tabela_campanha').DataTable({
    responsive: true,
    "order": [[ 3, 'desc' ]],
    language: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });
});
</script>
EOF;
include_once("template/footer.php");
?>
