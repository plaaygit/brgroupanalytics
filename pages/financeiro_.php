<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['campanhas']['status'] = true;
include_once("template/header.php");
$dados = get_campanhas();
?>
<div class="row row-pm">
  <h1 class="titulo">Campanhas <span><a href="<?php echo $page['criar_campanha'][0];?>">criar nova</a></span></h1>
  <div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
      <thead>
        <tr>
          <th>Nome da campanha</th>
          <th class="hidden-xs">Cliente</th>
          <th class="hidden-xs">Status</th>
          <th class="hidden-xs">Data de criação</th>
          <th class="hidden-xs">Prazo</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dados['posts'] as $campanha) {
          echo '<tr class="odd gradeX">
          <td><a href="'.$page['campanhas'][0].'/ver/'.$campanha['id'].'">'.$campanha['nome'].'</a></td>
          <td class="hidden-xs">'.$campanha['cliente'].'</td>
          <td class="hidden-xs">'.$campanha['status'].'</td>
          <td class="hidden-xs" data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['created']).'">'.date('d/m/Y', strtotime($campanha['created'])).'</td>
          <td class="hidden-xs" data-search="'.$campanha['created'].'" data-order="'.strtotime($campanha['prazo']).'">'.date('d/m/Y', strtotime($campanha['prazo'])).'</td>
          </tr>';
        }

        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
$footer = <<<EOF
<script>
$(document).ready(function() {
  $('#tabela_campanha').DataTable({
    responsive: true,
    "order": [[ 3, 'desc' ]],
    language: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });
});
</script>
EOF;
include_once("template/footer.php");
?>
