<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
include_once("../alterar_conteudo.php");
$page['editar_campanha']['status'] = true;
$info_campanha = get_campanhas($_GET['id']);
if($info_campanha == NULL){
  header("Location: ".$page['campanhas'][0]);
  die;
}
if(isset($_POST['nome'])){
  $id = $_POST['id'];
  $nome = $_POST['nome'];
  $cliente = $_POST['cliente'];
  $status = 0;
  $prazo = strtotime(str_replace("/", "-", $_POST['prazo']));
  $senha = $_POST['senha'];
  $campanha = editar_campanha($id, $nome, $cliente, $senha, $status, $prazo);
  $info_campanha['nome']=$_POST['nome'];
  $info_campanha['cliente']=$_POST['cliente'];
  $info_campanha['senha']=$_POST['senha'];
  $info_campanha['prazo']=$_POST['prazo'];
  $campanha= $_GET['id'];
  $controller['error'] = 1;
  $controller['message'] = "Campanha editada com sucesso. <a href='".$site_url."/campanha/ver/".$campanha."'>Ver campanha.</a>";
}
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Editar campanha</h1>
  <form role="form" id="form" method="post">
    <input class="form-control" name="id" value="<?php echo $info_campanha['id'];?>" style="display:none">
    <div class="form-group">
      <label>Nome</label>
      <input class="form-control" name="nome" value="<?php echo $info_campanha['nome'];?>" required>
      <p class="help-block">Nome da campanha.</p>
    </div>
    <div class="form-group">
      <label>Cliente</label>
      <input class="form-control" name="cliente" value="<?php echo $info_campanha['cliente'];?>" required>
      <p class="help-block">Nome do cliente que está realizando a campanha.</p>
    </div>
    <div class="form-group">
      <label>Senha</label>
      <input class="form-control" name="senha" value="<?php echo $info_campanha['senha'];?>" required>
      <p class="help-block">Senha para acessso da campanha.</p>
    </div>
    <div class="form-group">
      <label>Prazo</label>
      <input class="form-control" name="prazo" id="prazo" type="text" value="<?php echo $info_campanha['prazo'];?>" required>
      <p class="help-block">Prazo estimado para entrega do relatório.</p>
    </div>
    <button type="submit" class="btn btn-primary ">Salvar alterações</button>
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletar_campanha">Deletar campanha</button>
  </form>
</div>
<?php
$footer = <<<EOF
<div id="deletar_campanha" class="modal fade" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">DELETAR CAMPANHA</h4>
</div>
<div class="modal-body">
<center>
<p>Você tem certeza que deseja deletar essa campanha?</p><br>
<p>Essa ação é definitiva e irreversível.</p>
</center>
</div>
<div class="modal-footer">
<center>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
<button class="btn btn-danger btn_deletar_conta" data-idcampanha="{$info_campanha['id']}">Deletar</button>
</center>
</div>
</div>

</div>
</div>

<script>
  /* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */
(function( factory ) {
    if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define([ "../datepicker" ], factory );
    } else {

        // Browser globals
        factory( jQuery.datepicker );
    }
}(function( datepicker ) {

datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3C;Anterior',
    nextText: 'Próximo&#x3E;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['pt-BR']);

return datepicker.regional['pt-BR'];

}));
  $(function() {
    $( "#prazo"  ).datepicker( $.datepicker.regional[ "pt-BR" ] );
  });
  </script>
EOF;
include_once("template/footer.php");
?>
