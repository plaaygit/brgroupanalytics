<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
include_once("../inserir_conteudo.php");
$page['adicionar_post']['status'] = true;
$info_campanha = get_campanhas($_GET['id']);
if($info_campanha == NULL){
  header("Location: ".$page['campanhas'][0]);
  die;
}
$campanha = $_GET['id'];
if(isset($_POST['influenciador'])){
  $influenciador = $_POST['influenciador'];
  $username = $_POST['username'];
  $post_id =  $_POST['post_id'];
  $redesocial =  $_POST['redesocial'];
  $snap_views = $_POST['snap_views'];
  $snap_prints = $_POST['snap_prints'];
  $post = criar_post($influenciador, $username, $post_id, $snap_views, $snap_prints, $redesocial, $_GET['id']);
  $controller['error'] = 1;
  $controller['message'] = "Post cadastrado com sucesso! <a href='".$site_url."/campanha/ver/".$campanha."'>Ver campanha.</a>";
}
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Adicionar post</h1>
  <form role="form" id="form" method="post">
    <div class="form-group">
      <label>Influenciador</label>
      <input class="form-control" name="influenciador" required>
      <p class="help-block">Nome do influenciador.</p>
    </div>
    <div class="form-group">
      <label>Username</label>
      <input class="form-control" name="username" required>
      <p class="help-block">Username do influenciador.</p>
    </div>
    <div class="form-group" id="post_id">
      <label>Post ID</label>
      <input class="form-control" name="post_id" id="ipost" type="text" required>
      <p class="help-block">Id do post.</p>
    </div>
    <div class="form-group" id="viewsSnap" style="display:none;">
      <label>Views</label>
      <input class="form-control" name="snap_views" type="text" value="<?php echo $post['snap_views'];?>">
      <p class="help-block">Views do snap.</p>
    </div>
    <div class="form-group" id="screenShots" style="display:none;">
      <label>Screenshots</label>
      <input class="form-control" name="snap_prints" type="text" value="<?php echo $post['snap_prints'];?>">
      <p class="help-block">Prints do snap.</p>
    </div>
    <div class="form-group">
      <label  for="radios">Rede Social</label>
      <select id="selectbasic" name="redesocial" class="form-control" required>
        <option value="">Escolha a rede social do post</option>
        <?php
        $redes = array('twitter','facebook','instagram','youtube', 'snapchat');

        foreach($redes as $rede){
          $selected = '' ;
          echo '<option value="'.$rede.'">'.ucfirst($rede).'</option>';
        }?>
      </select>
    </div>
    <button type="submit" class="btn btn-primary ">Cadastrar post</button>
    <button type="reset" class="btn btn-default">Resetar</button>
  </form>
</div>
<?php
$footer = <<<EOF
<script>
/* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */
(function( factory ) {
  if ( typeof define === "function" && define.amd ) {

    // AMD. Register as an anonymous module.
    define([ "../datepicker" ], factory );
  } else {

    // Browser globals
    factory( jQuery.datepicker );
  }
}(function( datepicker ) {

  datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3C;Anterior',
    nextText: 'Próximo&#x3E;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
    datepicker.setDefaults(datepicker.regional['pt-BR']);

    return datepicker.regional['pt-BR'];

  }));
  $(function() {
    $( "#prazo"  ).datepicker( $.datepicker.regional[ "pt-BR" ] );
  });
  </script>
EOF;
include_once("template/footer.php");
?>
