<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['admin']['financeiro']['status'] = true;
$dados = get_faturas();
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Financeiro <span><a href="<?php echo $page['criar_campanha'][0];?>">criar nova fatura</a></span></h1>
  <div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="tabela_campanha">
      <thead>
        <tr>
          <th>Fatura</th>`x
          <th class="hidden-xs">Cliente</th>
          <th class="hidden-xs">Status</th>
          <th class="hidden-xs">Data de criação</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($dados['faturas'] as $fatura) {
          echo '<tr class="odd gradeX">
          <td><a href="'.$page['campanhas'][0].'/ver/'.$fatura['id'].'">'.$fatura['nome'].'</a></td>
          <td class="hidden-xs">'.$fatura['cliente'].'</td>
          <td class="hidden-xs">'.$fatura['status'].'</td>
          <td class="hidden-xs" data-search="'.$fatura['created'].'" data-order="'.strtotime($fatura['created']).'">'.date('d/m/Y', strtotime($fatura['created'])).'</td>
          </tr>';
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
$footer = <<<EOF
<script>
$(document).ready(function() {
  $('#tabela_campanha').DataTable({
    responsive: true,
    "order": [[ 3, 'desc' ]],
    language: {
      "sEmptyTable": "Nenhum registro encontrado",
      "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
      "sInfoFiltered": "(Filtrados de _MAX_ registros)",
      "sInfoPostFix": "",
      "sInfoThousands": ".",
      "sLengthMenu": "_MENU_ resultados por página",
      "sLoadingRecords": "Carregando...",
      "sProcessing": "Processando...",
      "sZeroRecords": "Nenhum registro encontrado",
      "sSearch": "Pesquisar",
      "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
      },
      "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
      }
    }
  });
});
</script>
EOF;
include_once("template/footer.php");
?>
