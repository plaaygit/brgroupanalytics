<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
$page['calendario']['status'] = true;
include_once("template/header.php");
$dados = get_campanhas();
?>
<div class="row row-pm">
  <h1 class="titulo">Calendário de entregas</h1>
  <div id="calendario">
<script>
var eventos = [
  <?php
    foreach ($dados['posts'] as $campanha) {
      $new_time = date("Y-m-d H:i:s", strtotime($campanha['prazo'])+3600);
      if($campanha['status'] != '0'){
   ?>
 {title: <?php echo json_encode($campanha['nome']);?>, start: '<?php echo $campanha['prazo'];?>',end: '<?php echo $new_time;?>'},
 <?php
 }}
 ?>

];
</script>
  </div>
</div>
<?php
$footer = <<<EOF
<script>

	$(document).ready(function() {
	var m = moment();
		$('#calendario').fullCalendar({
			header: {
				right: '',
				center: 'today prev,next',
				left: ''
			},
			defaultView: 'agendaWeek',
			defaultDate: m,
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			allDaySlot: false,
      scrollTime: '12:00:00',
      minTime: '10:00:00',
      maxTime: '20:00:00',
      contentHeight: 'auto',
			columnFormat: 'ddd\\n D/M',
			events: eventos,
			eventClick: function(calEvent, jsEvent, view) {

        alert('Event: ' + calEvent.title);
        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
        alert('View: ' + view.name);

        // change the border color just for fun
        $(this).css('border-color', 'red');

    }
		});

	});

</script>
EOF;
include_once("template/footer.php");
?>
