<?php
include_once("../functions.php");
include_once("../exibir_conteudo.php");
include_once("../alterar_conteudo.php");
$page['criar_campanha']['status'] = true;
if(isset($_POST['nome'])){
  $nome = $_POST['nome'];
  $cliente = $_POST['cliente'];
  $status = 0;
  $prazo = strtotime(str_replace("/", "-", $_POST['prazo']));;
  $campanha = criar_campanha($nome, $cliente, $status, $prazo);
  $controller['error'] = 1;
  $controller['message'] = "Campanha criada com sucesso. <a href='".$site_url."/campanha/ver/".$campanha."'>Ver campanha.</a>";
}
include_once("template/header.php");
?>
<div class="row row-pm">
  <h1 class="titulo">Criar campanha</h1>
  <form role="form" id="form" method="post">
    <div class="form-group">
      <label>Nome</label>
      <input class="form-control" name="nome" required>
      <p class="help-block">Nome da campanha.</p>
    </div>
    <div class="form-group">
      <label>Cliente</label>
      <input class="form-control" name="cliente" required>
      <p class="help-block">Nome do cliente que está realizando a campanha.</p>
    </div>
    <div class="form-group">
      <label>Prazo</label>
      <input class="form-control" name="prazo" id="prazo" type="text" required>
      <p class="help-block">Prazo estimado para entrega do relatório.</p>
    </div>
    <button type="submit" class="btn btn-primary ">Cadastrar campanha</button>
    <button type="reset" class="btn btn-default">Resetar</button>
  </form>
</div>
<?php
$footer = <<<EOF
<script>
  /* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */
(function( factory ) {
    if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define([ "../datepicker" ], factory );
    } else {

        // Browser globals
        factory( jQuery.datepicker );
    }
}(function( datepicker ) {

datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3C;Anterior',
    nextText: 'Próximo&#x3E;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['pt-BR']);

return datepicker.regional['pt-BR'];

}));
  $(function() {
    $( "#prazo"  ).datepicker( $.datepicker.regional[ "pt-BR" ] );
  });
  </script>
EOF;
include_once("template/footer.php");
?>
