// conteudo tamanho sidebar
// hamburger menu
$('#hamburger').click(function(){
  if ( $('#sidebar').hasClass('hamburger')){
    $('#display').removeClass('hamburger_content');
    $('#sidebar').removeClass('hamburger');
    $.removeCookie("hamburger");
  }else{
    $('#sidebar').addClass('hamburger');
    $('#display').addClass('hamburger_content');
    $.cookie("hamburger", 1);
  }
});

if($.cookie("hamburger")){
  $('#sidebar').addClass('notransition');
  $('#display').addClass('notransition');
  $('#sidebar').addClass('hamburger');
  $('#display').addClass('hamburger_content');
  setTimeout(function () {
    $('#sidebar').removeClass('notransition');
    $('#display').removeClass('notransition');
  }, 1000);
}

// hamburger display none mobile
if($(window).width() < 768){
  $('#sidebar').addClass('notransition');
  $('#display').addClass('notransition');
  $('#sidebar').addClass('hamburger');
  $('#display').addClass('hamburger_content');
  setTimeout(function () {
    $('#sidebar').removeClass('notransition');
    $('#display').removeClass('notransition');
  }, 1000);
}

// finalizar campanhas
$('#checkbox1').change(function() {
  if($(this).is(':checked')){
    $('#fechar_relatorio').modal('show');
  }
  $.post('http://relatorios.plaaymedia.com/pages/ajax/status_campanha.php', { campanha: id_campanha, status: $(this).is(':checked') }).success(function (data){
    $(".container-fluid").append('<div class="avisoSalvo animated bounceOut">Salvo</div>');
    setTimeout(function () {
      $(".avisoSalvo").remove();
    }, 2000);
  });
});
function removerDuplicados()
{
  $('[id]').each(function () {
      $('[id="' + this.id + '"]:gt(0)').remove();
  });
    setTimeout( removerDuplicados, 100 );
}



// avaliar comentário
function avaliar_comentario(id, sentimento){
  var i = 0;
  var anothers = new Array();
  $('.cover').each(function(){
    anothers[i]= $(this).attr('id');
    i++;
  });
  $('#'+id).remove();
  $.get('http://relatorios.plaaymedia.com/pages/ajax/avaliar_comentario.php?id='+id+'&sentimento='+sentimento).success(function (data){
    get_comentario(5, anothers);
    var restantes = $(".restantes").text();
    $(".restantes").html(restantes - 1);
    console.log('Comentario add com sucesso');
  });
}

function get_comentario(numero, anothers){
  if(anothers == 'null'){
    $.get( "http://relatorios.plaaymedia.com/pages/ajax/get-comentarios.php?data="+numero, function( data ) {
      $( ".comentarios" ).append( data );
      twemoji.parse(document.body,  {size: 36});
    });
  }else{
    if(typeof anothers[0] != 'undefined' && typeof anothers[1] != 'undefined' ){
      $.get( "http://relatorios.plaaymedia.com/pages/ajax/get-comentarios.php?data="+numero+"&outro1="+anothers[0]+"&outro2="+anothers[1], function( data ) {
        $( ".comentarios" ).append( data );
        twemoji.parse(document.body,  {size: 36});
      });
    }
    if(typeof anothers[0] != 'undefined' && typeof anothers[1] == 'undefined' ){
      $.get( "http://relatorios.plaaymedia.com/pages/ajax/get-comentarios.php?data="+numero+"&outro1="+anothers[0], function( data ) {
        $( ".comentarios" ).append( data );
        twemoji.parse(document.body,  {size: 36});
      });
    }
  }
}

// deletar campanha
$(function(){
  $(".btn_fechar_relatorio").click(function() {
    var id_campanha = $(this).attr("data-idcampanha");
    var prazo = $("#prazo").datepicker('getDate');
    prazo = moment(prazo).format("YYYY-MM-DD HH:mm:ss")
    var hora = $('#hora').data("DateTimePicker").viewDate()._d;
    hora = moment(hora).format("YYYY-MM-DD HH:mm:ss")
    $('#fechar_relatorio').modal('hide');
    $('#prazo').show();
    $('#hora').hide();
    $.post('http://relatorios.plaaymedia.com/pages/ajax/alterar_prazo.php', { campanha: id_campanha, prazo: prazo, hora: hora }).success(function (data){
      $(".container-fluid").append('<div class="avisoSalvo animated bounceOut">Salvo</div>');
      setTimeout(function () {
        $(".avisoSalvo").remove();
      }, 2000);    });
  });
  $(".btn_deletar_conta").click(function() {
    var id_campanha = $(this).attr("data-idcampanha");
    $.post('http://relatorios.plaaymedia.com/pages/ajax/deletar_campanha.php', { campanha: id_campanha }).success(function (data){
      window.location.href = 'http://relatorios.plaaymedia.com/campanhas'; //Redireciona para pagina de campanhas
    });
  });

  $(".btn_deletar_post").click(function() {
    var id_campanha = $(this).attr("data-idcampanha");
    var id_post = $(this).attr("data-idpost");
    $.post('http://relatorios.plaaymedia.com/pages/ajax/deletar_post.php', { campanha: id_campanha, post: id_post }).success(function (data){
      window.location.href = 'http://relatorios.plaaymedia.com/campanhas/ver/'+id_campanha; //Redireciona para pagina de campanhas
    });
  });

  // snapchat
  $('#selectbasic').bind('change', function (e) {
        if( $('#selectbasic').val() == 'snapchat') {
          $('#viewsSnap').show();
          $('#screenShots').show();
        }else{
          $('#viewsSnap').hide();
          $('#screenShots').hide();
        }
      }).trigger('change');
});
