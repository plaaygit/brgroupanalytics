var words = [
  {text: "Lorem", weight: 13},
	{text: "Ipsum", weight: 10.5},
	{text: "Dolor", weight: 9.4},
	{text: "Sit", weight: 8},
	{text: "Amet", weight: 6.2},
	{text: "Consectetur", weight: 5},
	{text: "Adipiscing", weight: 5},
	{text: "Elit", weight: 5},
	{text: "Nam et", weight: 5},
	{text: "Leo", weight: 4},
	{text: "Sapien", weight: 4},
	{text: "Pellentesque", weight: 3},
	{text: "habitant", weight: 3},
	{text: "morbi", weight: 3},
	{text: "tristisque", weight: 3},
	{text: "senectus", weight: 3},
	{text: "et netus", weight: 3},
	{text: "et malesuada", weight: 3},
	{text: "fames", weight: 2},
	{text: "ac turpis", weight: 2},
	{text: "egestas", weight: 2},
	{text: "Aenean", weight: 2},
	{text: "vestibulum", weight: 2},
	{text: "elit", weight: 2},
	{text: "sit amet", weight: 2},
	{text: "metus", weight: 2},
	{text: "adipiscing", weight: 2},
	{text: "ut ultrices", weight: 2},
	{text: "justo", weight: 1},
	{text: "dictum", weight: 1},
	{text: "Ut et leo", weight: 1},
	{text: "metus", weight: 1},
	{text: "at molestie", weight: 1},
	{text: "purus", weight: 1},
	{text: "Curabitur", weight: 1},
	{text: "diam", weight: 1},
	{text: "dui", weight: 1},
	{text: "ullamcorper", weight: 1},
	{text: "id vuluptate ut", weight: 1},
	{text: "mattis", weight: 1},
	{text: "et nulla", weight: 1},
	{text: "Sed", weight: 1}

];

var facebook_tags = ["#336699", "#4279b0", "#5893cd", "#6ea9e5"];
var twitter_tags = ["#00aeeb", "#12bcf7", "#33c9fe", "#52d1fd"];
var instagram_tags = ["#3f729b", "#5289b6", "#6ba6d5", "#7eb9e9"];
var youtube_tags = ["#cd201f", "#df3332", "#f14f4e", "#fb7c7b"];

$(document).ready(function(){

  $('#facebook').jQCloud(words, {
	classPattern: null,
	autoResize: true,
	colors: facebook_tags,
	fontSize: {
	  from: 0.1,
	  to: 0.02
	}
  });

  $('#twitter').jQCloud(words, {
	classPattern: null,
	autoResize: true,
	colors: twitter_tags,
	fontSize: {
	  from: 0.1,
	  to: 0.02
	}
  });

  $('#instagram').jQCloud(words, {
	classPattern: null,
	autoResize: true,
	colors: instagram_tags,
	fontSize: {
	  from: 0.1,
	  to: 0.02
	}
  });

  $('#youtube').jQCloud(words, {
	classPattern: null,
	autoResize: true,
	colors: youtube_tags,
	fontSize: {
	  from: 0.1,
	  to: 0.02
	}
  });

    CanvasJS.addColorSet("blueshades",["#9daccb","#3b5998","#768bb7"]);

	var chart = new CanvasJS.Chart("chartContainer",
    {
		colorSet: "blueshades",
		animationEnabled: true,
  		data: [
  		{
  			type: "pie",
  			showInLegend: false,
  			toolTipContent: "{legendText}: <strong>{y}%</strong>",
  			indexLabel: "{label} {y}%",
  			dataPoints: [
  				{  y: 20, legendText: "Neutros", label: "Neutros" },
  				{  y: 60, legendText: "Positivos", label: "Positivos" },
  				{  y: 20, legendText: "Negativos", label: "Negativos" }
  			]
  	  }
  	 ]
  	});
  	chart.render();

});
